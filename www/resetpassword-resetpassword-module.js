(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["resetpassword-resetpassword-module"],{

/***/ "./src/app/resetpassword/resetpassword.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/resetpassword/resetpassword.module.ts ***!
  \*******************************************************/
/*! exports provided: ResetpasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpasswordPageModule", function() { return ResetpasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _resetpassword_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./resetpassword.page */ "./src/app/resetpassword/resetpassword.page.ts");







var routes = [
    {
        path: '',
        component: _resetpassword_page__WEBPACK_IMPORTED_MODULE_6__["ResetpasswordPage"]
    }
];
var ResetpasswordPageModule = /** @class */ (function () {
    function ResetpasswordPageModule() {
    }
    ResetpasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_resetpassword_page__WEBPACK_IMPORTED_MODULE_6__["ResetpasswordPage"]]
        })
    ], ResetpasswordPageModule);
    return ResetpasswordPageModule;
}());



/***/ }),

/***/ "./src/app/resetpassword/resetpassword.page.html":
/*!*******************************************************!*\
  !*** ./src/app/resetpassword/resetpassword.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-content>\r\n        <ion-card  color=\"#d7eceb\" class=\"message\">\r\n            <img style=\"width: 50%; margin-left: 25%; margin-top:2%\" src=\"assets\\icons\\inquizity.png\"/>\r\n            <ion-card-header *ngIf=\"!showLogin\">\r\n                <ion-card-title text-center>Password Reset Page</ion-card-title>\r\n              </ion-card-header>\r\n              <ion-card-content *ngIf=\"!showLogin\">\r\n                  <form  #resetPasswordForm=\"ngForm\" (ngSubmit)=\"resetPasswordForm.form.valid && resetPassword(resetPasswordForm.value)\" autocomplete=\"off\" novalidate>  \r\n                      <ion-item>\r\n                          <ion-label>  <ion-icon name=\"calculator\"></ion-icon></ion-label>\r\n                                     <ion-input placeholder=\"otpcode\" name=\"code\" type=\"number\" required ngModel ></ion-input>\r\n                      </ion-item>      \r\n                    <ion-item class=\"i2\"no-lines>\r\n                            <ion-label >  <ion-icon name=\"unlock\"></ion-icon> </ion-label>\r\n                        <ion-input type=\"{{type}}\" name=\"password\"  placeholder=\"New Password..\" required ngModel style=\"width: 40px;\" no-lines>\r\n                      </ion-input>\r\n                        <ion-icon *ngIf=\"!showPass\" item-right (click)=\"showPassword()\" name=\"eye\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\r\n                        <ion-icon *ngIf=\"showPass\" item-right (click)=\"showPassword()\" name=\"eye-off\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\r\n                    </ion-item>\r\n                    <ion-item class=\"i2\"no-lines>\r\n                        <ion-label >  <ion-icon name=\"unlock\"></ion-icon> </ion-label>\r\n                    <ion-input type=\"{{type}}\" name=\"confirmpassword\" placeholder=\"Confirm Password..\" required ngModel style=\"width: 40px;\" no-lines>\r\n                  </ion-input>\r\n                  <ion-icon *ngIf=\"!showPass\" item-right (click)=\"showPassword()\" name=\"eye\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\r\n                  <ion-icon *ngIf=\"showPass\" item-right (click)=\"showPassword()\" name=\"eye-off\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\r\n                </ion-item>\r\n                  <div *ngIf=\"error\" class=\"alert alert-danger\" style=\"color: green;\">{{error}}</div>\r\n                  <br>\r\n                  <div *ngIf=\"showLogin\"> Your password is updated</div>\r\n                  <ion-button expand=\"block\" position=\"center\" class=\"submit-btn\" [disabled]=\"!resetPasswordForm.form.valid\" full type=\"submit\">\r\n                      Submit\r\n                  </ion-button>\r\n                  <div *ngIf=\"show\" id=\"cover\" >\r\n                      <div ng-show=\"showLoader\" class=\"center\">\r\n                      <svg width=\"55px\"  height=\"115px\"  xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\" class=\"lds-ellipsis\" style=\"background: none;\"><!--circle(cx=\"16\",cy=\"50\",r=\"10\")--><circle cx=\"84\" cy=\"50\" r=\"0\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"10;0;0;0;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"84;84;84;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"42.6958\" cy=\"50\" r=\"10\" fill=\"#7de6ea\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate></circle><circle cx=\"16\" cy=\"50\" r=\"7.85172\" fill=\"#ffa570\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate></circle><circle cx=\"84\" cy=\"50\" r=\"2.14828\" fill=\"#d4e683\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"76.6958\" cy=\"50\" r=\"10\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"0;0;10;10;10\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;16;50;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle></svg>\r\n                  \r\n                            </div>\r\n                      </div>  \r\n                  \r\n              </form>\r\n            </ion-card-content>\r\n            <a href=\"#\" *ngIf=\"showLogin\"><ion-button>Go to Login</ion-button></a>\r\n          </ion-card>\r\n          \r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/resetpassword/resetpassword.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/resetpassword/resetpassword.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #ffb01a; }\n\nion-title {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  text-align: center; }\n\n.submit-btn {\n  --background: #ffb01a;\n  --color: #161b12;\n  position: center; }\n\n@media all and (max-width: 600px) {\n  .message {\n    background-image: linear-gradient(200deg, #7579ff, #b224ef 70%);\n    background: -webkit-linear-gradient(top, #ffffff, #fdf8ff);\n    position: absolute;\n    transform: translate(0%, 49%);\n    width: 86%;\n    text-align: center;\n    padding: 1%;\n    box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); } }\n\n@media all and (min-width: 600px) {\n  .message {\n    background-image: linear-gradient(200deg, #7579ff, #b224ef 70%);\n    background: -webkit-linear-gradient(top, #ffffff, #fdf8ff);\n    position: absolute;\n    transform: translate(89%, 18%);\n    width: 35%;\n    text-align: center;\n    padding: 3%;\n    box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); } }\n\n#cover {\n  position: fixed;\n  top: 52%;\n  left: 0;\n  z-index: 5;\n  width: 100%;\n  height: 500%;\n  cursor: wait; }\n\n.messages {\n  position: absolute;\n  transform: translate(285%, 200%);\n  width: 15%;\n  text-align: center;\n  padding: 3%; }\n\n@media all and (max-width: 600px) {\n  .video {\n    width: 164%;\n    height: 327px;\n    margin-left: -118px; }\n  .message1 {\n    display: none;\n    position: absolute;\n    transform: translate(-1%, 64%);\n    width: 102%;\n    text-align: center;\n    padding: 1%;\n    /* box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); */ } }\n\n@media all and (min-width: 600px) {\n  .video {\n    width: 100%;\n    height: 349px; }\n  .message1 {\n    background-image: white; } }\n\n@media all and (max-width: 600px) {\n  .video {\n    width: 164%;\n    height: 327px;\n    margin-left: -118px; }\n  .message2 {\n    display: block;\n    /* box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); */ } }\n\n@media all and (min-width: 600px) {\n  .video {\n    width: 100%;\n    height: 349px; }\n  .message2 {\n    display: none; } }\n\n/*:host {\r\n    ion-content {\r\n        --background: linear-gradient(135deg, var(--ion-color-dark), var(--ion-color-primary));\r\n    }\r\n}\r\n\r\n.paz {\r\n    position: relative;\r\n    z-index: 10;\r\n}\r\n*/\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVzZXRwYXNzd29yZC9FOlxcS29tYWxcXEQgRHJpdmVcXElvbmljIFByb2plY3RzXFxJb25pY19HcmFzaW1QV0FcXGdyYXNpbV9pb25pY3B3YS9zcmNcXGFwcFxccmVzZXRwYXNzd29yZFxccmVzZXRwYXNzd29yZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3Jlc2V0cGFzc3dvcmQvcmVzZXRwYXNzd29yZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBYSxFQUFBOztBQUdqQjtFQUNJLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFdBQVc7RUFDWCxZQUFZO0VBQ1osa0JBQWtCLEVBQUE7O0FBRXRCO0VBQ0kscUJBQWE7RUFDYixnQkFBUTtFQUNSLGdCQUFnQixFQUFBOztBQUVwQjtFQUVJO0lBRUksK0RBQTZEO0lBQ3pELDBEQUEwRDtJQUMxRCxrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLDZFQUF3RSxFQUFBLEVBQzNFOztBQUVMO0VBRUk7SUFFSSwrREFBNkQ7SUFDekQsMERBQTBEO0lBQzFELGtCQUFrQjtJQUNsQiw4QkFBOEI7SUFDOUIsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsNkVBQXdFLEVBQUEsRUFDM0U7O0FBR0w7RUFDSSxlQUFjO0VBQ2QsUUFBTztFQUNQLE9BQU07RUFDTixVQUFTO0VBQ1QsV0FBVTtFQUNWLFlBQVc7RUFFWCxZQUFZLEVBQUE7O0FBR2hCO0VBRUksa0JBQWtCO0VBQ2xCLGdDQUFnQztFQUNwQixVQUFVO0VBQ3RCLGtCQUFrQjtFQUNsQixXQUFXLEVBQUE7O0FBR2Y7RUFFSTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsbUJBQW1CLEVBQUE7RUFHdkI7SUFJTyxhQUFZO0lBQ1osa0JBQWtCO0lBRXpCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxtRkFBQSxFQUFvRixFQUMvRTs7QUFFTDtFQUVJO0lBQ0ksV0FBVztJQUNYLGFBQWEsRUFBQTtFQUVqQjtJQUNJLHVCQUF1QixFQUFBLEVBVzFCOztBQUdMO0VBRUk7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLG1CQUFtQixFQUFBO0VBR3ZCO0lBSU8sY0FBYztJQUNyQixtRkFBQSxFQUFvRixFQUMvRTs7QUFFTDtFQUVJO0lBQ0ksV0FBVztJQUNYLGFBQWEsRUFBQTtFQUVqQjtJQUVHLGFBQVksRUFBQSxFQVdkOztBQUdyQjs7Ozs7Ozs7OztDQzdDQyIsImZpbGUiOiJzcmMvYXBwL3Jlc2V0cGFzc3dvcmQvcmVzZXRwYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmV3LWJhY2tncm91bmQtY29sb3Ige1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZiMDFhO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5zdWJtaXQtYnRuIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2ZmYjAxYTtcclxuICAgIC0tY29sb3I6ICMxNjFiMTI7XHJcbiAgICBwb3NpdGlvbjogY2VudGVyO1xyXG59XHJcbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSBcclxue1xyXG4gICAgLm1lc3NhZ2V7XHJcbiAgICAgICAgLy9iYWNrZ3JvdW5kLWltYWdlOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCM3NTc5ZmYsI2IyMjRlZiA3MCUpO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYsICNmZGY4ZmYpO1xyXG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDAlLCA0OSUpO1xyXG4gICAgICAgICAgICB3aWR0aDogODYlO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDElO1xyXG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwwLDAsMC4zMCksIDAgMTVweCAxMnB4IHJnYmEoMCwwLDAsMC4yMik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNjAwcHgpIFxyXG4gICAge1xyXG4gICAgICAgIC5tZXNzYWdle1xyXG4gICAgICAgICAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHdoaXRlO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCM3NTc5ZmYsI2IyMjRlZiA3MCUpO1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjZmZmZmZmLCAjZmRmOGZmKTtcclxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgMTglKTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAzNSU7XHJcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAzJTtcclxuICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLDAsMCwwLjMwKSwgMCAxNXB4IDEycHggcmdiYSgwLDAsMCwwLjIyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgI2NvdmVye1xyXG4gICAgICAgICAgICBwb3NpdGlvbjpmaXhlZDtcclxuICAgICAgICAgICAgdG9wOjUyJTtcclxuICAgICAgICAgICAgbGVmdDowO1xyXG4gICAgICAgICAgICB6LWluZGV4OjU7XHJcbiAgICAgICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgICAgIGhlaWdodDo1MDAlO1xyXG4gICAgICAgICAgICAvL2Rpc3BsYXk6bm9uZTtcclxuICAgICAgICAgICAgY3Vyc29yOiB3YWl0O1xyXG4gICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICAubWVzc2FnZXN7XHJcbiAgICAgICBcclxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgyODUlLCAyMDAlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE1JTtcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAzJTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIEBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSBcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC52aWRlb3tcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxNjQlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzMjdweDtcclxuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTE4cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC5tZXNzYWdlMXtcclxuICAgICAgICAgICAgICAgIC8vYmFja2dyb3VuZC1pbWFnZTogd2hpdGU7XHJcbiAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCgyMDBkZWcsIzc1NzlmZiwjYjIyNGVmIDcwJSk7XHJcbiAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYsICNmZGY4ZmYpO1xyXG4gICAgICAgICAgICAgICAgICAgZGlzcGxheTpub25lO1xyXG4gICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDQlLCAyNSUpO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMSUsIDY0JSk7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDIlO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDElO1xyXG4gICAgICAgICAgICAvKiBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgKi9cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkgXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIC52aWRlb3tcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDM0OXB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLm1lc3NhZ2Uxe1xyXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHdoaXRlO1xyXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYsICNmZGY4ZmYpO1xyXG4gICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgIC8vIG92ZXJmbG93OiBhdXRvO1xyXG4gICAgICAgICAgICAvLyAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgNDklKTtcclxuICAgICAgICAgICAgLy8gdHJhbnNmb3JtOiB0cmFuc2xhdGUoNTklLCAyNiUpO1xyXG4gICAgICAgICAgICAvLyB3aWR0aDogNDYlO1xyXG4gICAgICAgICAgICAvLyBoZWlnaHQ6IDE3JTtcclxuICAgICAgICAgICAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAzJTtcclxuICAgICAgICAgICAgLy8gYm94LXNoYWRvdzogMHB4IDE5cHggMzhweCByZ2JhKDAsIDAsIDAsIDAuMyksIDAgMTVweCAxMnB4IHJnYmEoMCwgMCwgMCwgMC4yMik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIEBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgLnZpZGVve1xyXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNjQlO1xyXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMzI3cHg7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMThweDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgLm1lc3NhZ2Uye1xyXG4gICAgICAgICAgICAgICAgICAgIC8vYmFja2dyb3VuZC1pbWFnZTogd2hpdGU7XHJcbiAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCM3NTc5ZmYsI2IyMjRlZiA3MCUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgICAgICAvKiBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgKi9cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkgXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgLnZpZGVve1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAzNDlweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLm1lc3NhZ2Uye1xyXG4gICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6bm9uZTtcclxuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XHJcbiAgICAgICAgICAgICAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgICAgICAgICAvLyBvdmVyZmxvdzogYXV0bztcclxuICAgICAgICAgICAgICAgIC8vIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoODklLCA0OSUpO1xyXG4gICAgICAgICAgICAgICAgLy8gdHJhbnNmb3JtOiB0cmFuc2xhdGUoNTklLCAyNiUpO1xyXG4gICAgICAgICAgICAgICAgLy8gd2lkdGg6IDQ2JTtcclxuICAgICAgICAgICAgICAgIC8vIGhlaWdodDogMTclO1xyXG4gICAgICAgICAgICAgICAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgLy8gcGFkZGluZzogMyU7XHJcbiAgICAgICAgICAgICAgICAvLyBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbi8qOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucGF6IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDEwO1xyXG59XHJcbiovXHJcbiIsIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmYjAxYTsgfVxuXG5pb24tdGl0bGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5zdWJtaXQtYnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZiMDFhO1xuICAtLWNvbG9yOiAjMTYxYjEyO1xuICBwb3NpdGlvbjogY2VudGVyOyB9XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5tZXNzYWdlIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCAjNzU3OWZmLCAjYjIyNGVmIDcwJSk7XG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjZmZmZmZmLCAjZmRmOGZmKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCUsIDQ5JSk7XG4gICAgd2lkdGg6IDg2JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMSU7XG4gICAgYm94LXNoYWRvdzogMHB4IDE5cHggMzhweCByZ2JhKDAsIDAsIDAsIDAuMyksIDAgMTVweCAxMnB4IHJnYmEoMCwgMCwgMCwgMC4yMik7IH0gfVxuXG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xuICAubWVzc2FnZSB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDIwMGRlZywgIzc1NzlmZiwgI2IyMjRlZiA3MCUpO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgMTglKTtcbiAgICB3aWR0aDogMzUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAzJTtcbiAgICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgfSB9XG5cbiNjb3ZlciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiA1MiU7XG4gIGxlZnQ6IDA7XG4gIHotaW5kZXg6IDU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwMCU7XG4gIGN1cnNvcjogd2FpdDsgfVxuXG4ubWVzc2FnZXMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDI4NSUsIDIwMCUpO1xuICB3aWR0aDogMTUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDMlOyB9XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC52aWRlbyB7XG4gICAgd2lkdGg6IDE2NCU7XG4gICAgaGVpZ2h0OiAzMjdweDtcbiAgICBtYXJnaW4tbGVmdDogLTExOHB4OyB9XG4gIC5tZXNzYWdlMSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSg0JSwgMjUlKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMSUsIDY0JSk7XG4gICAgd2lkdGg6IDEwMiU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDElO1xuICAgIC8qIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpOyAqLyB9IH1cblxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNjAwcHgpIHtcbiAgLnZpZGVvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDM0OXB4OyB9XG4gIC5tZXNzYWdlMSB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogd2hpdGU7IH0gfVxuXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xuICAudmlkZW8ge1xuICAgIHdpZHRoOiAxNjQlO1xuICAgIGhlaWdodDogMzI3cHg7XG4gICAgbWFyZ2luLWxlZnQ6IC0xMThweDsgfVxuICAubWVzc2FnZTIge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIC8qIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpOyAqLyB9IH1cblxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNjAwcHgpIHtcbiAgLnZpZGVvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDM0OXB4OyB9XG4gIC5tZXNzYWdlMiB7XG4gICAgZGlzcGxheTogbm9uZTsgfSB9XG5cbi8qOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucGF6IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDEwO1xyXG59XHJcbiovXG4iXX0= */"

/***/ }),

/***/ "./src/app/resetpassword/resetpassword.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/resetpassword/resetpassword.page.ts ***!
  \*****************************************************/
/*! exports provided: ResetpasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpasswordPage", function() { return ResetpasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_Login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Services/Login.service */ "./src/app/Services/Login.service.ts");
/* harmony import */ var _Services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/data.service */ "./src/app/Services/data.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





var ResetpasswordPage = /** @class */ (function () {
    function ResetpasswordPage(loginservice, events, data) {
        this.loginservice = loginservice;
        this.events = events;
        this.data = data;
        this.showLogin = false;
        this.type = 'password';
        this.showPass = false;
        this.error = " ";
    }
    ResetpasswordPage.prototype.showPassword = function () {
        this.showPass = !this.showPass;
        if (this.showPass) {
            this.type = 'text';
        }
        else {
            this.type = 'password';
        }
    };
    ResetpasswordPage.prototype.resetPassword = function (form) {
        console.log(form);
        var Code = form.code;
        var NewPassword = form.password;
        var testpassword2 = form.confirmpassword;
        var resetdata = {
            NewPassword: NewPassword, Code: Code
        };
        alert(testpassword2 + " " + NewPassword);
        console.log(NewPassword);
        debugger;
        if (NewPassword === testpassword2) {
            this.error = " ";
            alert('Your password has been updated');
            var jsondata = JSON.stringify(resetdata);
            alert(jsondata);
            this.showLogin = true;
            this.loginservice.resetPassword(resetdata).subscribe(function (data) {
                console.log(data);
            });
        }
        else {
            this.error = "Password is not matching.";
            this.showLogin = false;
        }
    };
    ResetpasswordPage.prototype.ngOnInit = function () {
    };
    ResetpasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-resetpassword',
            template: __webpack_require__(/*! ./resetpassword.page.html */ "./src/app/resetpassword/resetpassword.page.html"),
            styles: [__webpack_require__(/*! ./resetpassword.page.scss */ "./src/app/resetpassword/resetpassword.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Services_Login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"],
            _Services_data_service__WEBPACK_IMPORTED_MODULE_3__["Data"]])
    ], ResetpasswordPage);
    return ResetpasswordPage;
}());



/***/ })

}]);
//# sourceMappingURL=resetpassword-resetpassword-module.js.map