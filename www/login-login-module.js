(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.html":
/*!***************************************!*\
  !*** ./src/app/login/login.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<ion-header>\n  <ion-toolbar>\n    <ion-title>login</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-button expand=\"block\" routerLink=\"/menu/first\">Login</ion-button>\n</ion-content>-->\n<!-- <ion-header>\n    <ion-toolbar class=\"new-background-color\" align-title=\"center\">\n        <ion-title >Login</ion-title>\n    </ion-toolbar>\n</ion-header> -->\n\n\n\n<ion-content padding>\n    <!-- Loader Starts-->\n    <div *ngIf=\"show\" id=\"cover\">\n        <div ng-show=\"showLoader\" class=\"center\">\n            <div class=\"message\">\n                Please wait ... <br>\n                <svg width=\"55px\" height=\"67px\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 100 100\" preserveAspectRatio=\"xMidYMid\" class=\"lds-ellipsis\" style=\"background: none;\"><!--circle(cx=\"16\",cy=\"50\",r=\"10\")--><circle cx=\"84\" cy=\"50\" r=\"0\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"10;0;0;0;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"84;84;84;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"42.6958\" cy=\"50\" r=\"10\" fill=\"#7de6ea\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.7s\"></animate></circle><circle cx=\"16\" cy=\"50\" r=\"7.85172\" fill=\"#ffa570\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"-0.35s\"></animate></circle><circle cx=\"84\" cy=\"50\" r=\"2.14828\" fill=\"#d4e683\"><animate attributeName=\"r\" values=\"0;10;10;10;0\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;50;84;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle><circle cx=\"76.6958\" cy=\"50\" r=\"10\" fill=\"#ffcc33\"><animate attributeName=\"r\" values=\"0;0;10;10;10\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate><animate attributeName=\"cx\" values=\"16;16;16;50;84\" keyTimes=\"0;0.25;0.5;0.75;1\" keySplines=\"0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1\" calcMode=\"spline\" dur=\"1.4s\" repeatCount=\"indefinite\" begin=\"0s\"></animate></circle></svg>\n\n            </div>\n        </div>\n    </div>\n    <!-- Loader Ends-->\n\n\n    <ion-card color=\"#d7eceb\" class=\"message\">\n        <!-- <ion-card-header style=\"    font-size: x-large;\n        font-weight: 600;\">\n          Data Savi\n        </ion-card-header> -->\n        <img style=\"width: 50%; margin-left: 25%; margin-top:2%\" src=\"assets\\icon\\inquizity.png\" />\n        <ion-card-content>\n            <form #loginForm=\"ngForm\" (ngSubmit)=\"loginUser($event)\" autocomplete=\"off\">\n\n                <ion-item>\n                    <ion-label>  <ion-icon name=\"person\"></ion-icon></ion-label>\n                    <!--<ion-input name=\"username\" id=\"loginField\"\n                               type=\"text\" required [(ngModel)]=\"username\" #email></ion-input>-->\n                    <ion-input name=\"username\" id=\"loginField\" placeholder=\"Enter the UserName\"\n                               type=\"text\" required ngModel></ion-input>\n                </ion-item>\n                <!--<ion-item>\n                    <ion-label >  <ion-icon name=\"unlock\"></ion-icon> </ion-label>\n                  <ion-input name=\"password\" id=\"passwordField\"\n                               type=\"password\" required [(ngModel)]=\"password\"></ion-input>\n                               <ion-input name=\"password\" id=\"passwordField\" placeholder=\"************\"\n                               type=\"password\" required ngModel></ion-input>\n                </ion-item>-->\n\n                <ion-item class=\"i2\" no-lines>\n                    <ion-label>  <ion-icon name=\"unlock\"></ion-icon> </ion-label>\n                    <ion-input type=\"{{type}}\" name=\"password\" id=\"passwordField\" placeholder=\"************\" type=\"password\" required ngModel style=\"width: 40px;\" no-lines>\n                    </ion-input>\n                    <button *ngIf=\"!showPass\" type=\"button\" item-right (click)=\"showPassword()\" style=\"   background: transparent;\">\n                        <ion-icon name=\"eye\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\n                    </button>\n                    <button *ngIf=\"showPass\" type=\"button\" item-right (click)=\"showPassword()\" style=\"   background: transparent;\">\n                        <ion-icon name=\"eye-off\" style=\"font-size:25px;    background: transparent;\"></ion-icon>\n\n                    </button>\n                </ion-item>\n\n\n\n                <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\n                <br>\n\n                <ion-button s\n                            expand=\"block\" position=\"center\" class=\"submit-btn\" full type=\"submit\"\n                            [disabled]=\"!loginForm.form.valid\" routerLink=\"/rolepage/first\" routerDirection=\"forward\">\n                    Login\n                </ion-button>\n\n                <!-- <button mat-raised-button color=\"primary\"\n                  [mdePopoverTriggerFor]=\"appPopover\"\n                  mdePopoverTriggerOn=\"click\" #popoverTrigger=\"mdePopoverTrigger\">\n                  Show Popover\n                </button>\n\n                <mde-popover  [mdePopoverOverlapTrigger]=\"true\">\n                  <mat-card style=\"max-width: 300px\">\n                    <mat-card-content>\n                    A material design popover component created using Angular version 8 with the Angular CDK.\n                    </mat-card-content>\n                  </mat-card>\n                </mde-popover> -->\n            </form>\n\n\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".new-background-color {\n  --background: #ffb01a; }\n\nion-title {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  text-align: center; }\n\n.submit-btn {\n  --background: #ffb01a;\n  --color: #161b12;\n  position: center; }\n\n@media all and (max-width: 600px) {\n  .message {\n    background-image: linear-gradient(200deg, #7579ff, #b224ef 70%);\n    background: -webkit-linear-gradient(top, #ffffff, #fdf8ff);\n    position: absolute;\n    transform: translate(0%, 49%);\n    width: 86%;\n    text-align: center;\n    padding: 1%;\n    box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); } }\n\n@media all and (min-width: 600px) {\n  .message {\n    background-image: linear-gradient(200deg, #7579ff, #b224ef 70%);\n    background: -webkit-linear-gradient(top, #ffffff, #fdf8ff);\n    position: absolute;\n    transform: translate(89%, 18%);\n    width: 35%;\n    text-align: center;\n    padding: 3%;\n    box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); } }\n\n#cover {\n  position: fixed;\n  top: 52%;\n  left: 0;\n  z-index: 5;\n  width: 100%;\n  height: 500%;\n  cursor: wait; }\n\n.messages {\n  position: absolute;\n  transform: translate(285%, 200%);\n  width: 15%;\n  text-align: center;\n  padding: 3%; }\n\n@media all and (max-width: 600px) {\n  .video {\n    width: 164%;\n    height: 327px;\n    margin-left: -118px; }\n  .message1 {\n    display: none;\n    position: absolute;\n    transform: translate(-1%, 64%);\n    width: 102%;\n    text-align: center;\n    padding: 1%;\n    /* box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); */ } }\n\n@media all and (min-width: 600px) {\n  .video {\n    width: 100%;\n    height: 349px; }\n  .message1 {\n    background-image: white; } }\n\n@media all and (max-width: 600px) {\n  .video {\n    width: 164%;\n    height: 327px;\n    margin-left: -118px; }\n  .message2 {\n    display: block;\n    /* box-shadow: 0px 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22); */ } }\n\n@media all and (min-width: 600px) {\n  .video {\n    width: 100%;\n    height: 349px; }\n  .message2 {\n    display: none; } }\n\n/*:host {\n    ion-content {\n        --background: linear-gradient(135deg, var(--ion-color-dark), var(--ion-color-primary));\n    }\n}\n\n.paz {\n    position: relative;\n    z-index: 10;\n}\n*/\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vRTpcXEtvbWFsXFxEIERyaXZlXFxJb25pYyBQcm9qZWN0c1xcSW9uaWNfR3Jhc2ltUFdBXFxncmFzaW1faW9uaWNwd2Evc3JjXFxhcHBcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFhLEVBQUE7O0FBR2pCO0VBQ0ksa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0IsRUFBQTs7QUFFdEI7RUFDSSxxQkFBYTtFQUNiLGdCQUFRO0VBQ1IsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBRUk7SUFFSSwrREFBNkQ7SUFDekQsMERBQTBEO0lBQzFELGtCQUFrQjtJQUNsQiw2QkFBNkI7SUFDN0IsVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsNkVBQXdFLEVBQUEsRUFDM0U7O0FBRUw7RUFFSTtJQUVJLCtEQUE2RDtJQUN6RCwwREFBMEQ7SUFDMUQsa0JBQWtCO0lBQ2xCLDhCQUE4QjtJQUM5QixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCw2RUFBd0UsRUFBQSxFQUMzRTs7QUFHTDtFQUNJLGVBQWM7RUFDZCxRQUFPO0VBQ1AsT0FBTTtFQUNOLFVBQVM7RUFDVCxXQUFVO0VBQ1YsWUFBVztFQUVYLFlBQVksRUFBQTs7QUFHaEI7RUFFSSxrQkFBa0I7RUFDbEIsZ0NBQWdDO0VBQ3BCLFVBQVU7RUFDdEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHZjtFQUVJO0lBQ0ksV0FBVztJQUNYLGFBQWE7SUFDYixtQkFBbUIsRUFBQTtFQUd2QjtJQUlPLGFBQVk7SUFDWixrQkFBa0I7SUFFekIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsV0FBVztJQUNYLG1GQUFBLEVBQW9GLEVBQy9FOztBQUVMO0VBRUk7SUFDSSxXQUFXO0lBQ1gsYUFBYSxFQUFBO0VBRWpCO0lBQ0ksdUJBQXVCLEVBQUEsRUFXMUI7O0FBR0w7RUFFSTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsbUJBQW1CLEVBQUE7RUFHdkI7SUFJTyxjQUFjO0lBQ3JCLG1GQUFBLEVBQW9GLEVBQy9FOztBQUVMO0VBRUk7SUFDSSxXQUFXO0lBQ1gsYUFBYSxFQUFBO0VBRWpCO0lBRUcsYUFBWSxFQUFBLEVBV2Q7O0FBR3JCOzs7Ozs7Ozs7O0NDN0NDIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5ldy1iYWNrZ3JvdW5kLWNvbG9yIHtcbiAgICAtLWJhY2tncm91bmQ6ICNmZmIwMWE7XG59XG5cbmlvbi10aXRsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uc3VibWl0LWJ0biB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZmZiMDFhO1xuICAgIC0tY29sb3I6ICMxNjFiMTI7XG4gICAgcG9zaXRpb246IGNlbnRlcjtcbn1cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSBcbntcbiAgICAubWVzc2FnZXtcbiAgICAgICAgLy9iYWNrZ3JvdW5kLWltYWdlOiB3aGl0ZTtcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDIwMGRlZywjNzU3OWZmLCNiMjI0ZWYgNzAlKTtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwJSwgNDklKTtcbiAgICAgICAgICAgIHdpZHRoOiA4NiU7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBwYWRkaW5nOiAxJTtcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLDAsMCwwLjMwKSwgMCAxNXB4IDEycHggcmdiYSgwLDAsMCwwLjIyKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkgXG4gICAge1xuICAgICAgICAubWVzc2FnZXtcbiAgICAgICAgICAgIC8vYmFja2dyb3VuZC1pbWFnZTogd2hpdGU7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCM3NTc5ZmYsI2IyMjRlZiA3MCUpO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgMTglKTtcbiAgICAgICAgICAgICAgICB3aWR0aDogMzUlO1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAzJTtcbiAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwwLDAsMC4zMCksIDAgMTVweCAxMnB4IHJnYmEoMCwwLDAsMC4yMik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAjY292ZXJ7XG4gICAgICAgICAgICBwb3NpdGlvbjpmaXhlZDtcbiAgICAgICAgICAgIHRvcDo1MiU7XG4gICAgICAgICAgICBsZWZ0OjA7XG4gICAgICAgICAgICB6LWluZGV4OjU7XG4gICAgICAgICAgICB3aWR0aDoxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OjUwMCU7XG4gICAgICAgICAgICAvL2Rpc3BsYXk6bm9uZTtcbiAgICAgICAgICAgIGN1cnNvcjogd2FpdDtcbiAgICAgICAgXG4gICAgICAgIH1cbiAgICAgICAgLm1lc3NhZ2Vze1xuICAgICAgIFxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMjg1JSwgMjAwJSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTUlO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgcGFkZGluZzogMyU7XG4gICAgICAgIH1cblxuICAgICAgICBAbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA2MDBweCkgXG4gICAgICAgIHtcbiAgICAgICAgICAgIC52aWRlb3tcbiAgICAgICAgICAgICAgICB3aWR0aDogMTY0JTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDMyN3B4O1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTE4cHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC5tZXNzYWdlMXtcbiAgICAgICAgICAgICAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHdoaXRlO1xuICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDIwMGRlZywjNzU3OWZmLCNiMjI0ZWYgNzAlKTtcbiAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYsICNmZGY4ZmYpO1xuICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6bm9uZTtcbiAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlKDQlLCAyNSUpO1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTElLCA2NCUpO1xuICAgICAgICAgICAgd2lkdGg6IDEwMiU7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBwYWRkaW5nOiAxJTtcbiAgICAgICAgICAgIC8qIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpOyAqL1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIEBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDYwMHB4KSBcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAudmlkZW97XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDM0OXB4O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAubWVzc2FnZTF7XG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHdoaXRlO1xuICAgICAgICAgICAgLy8gYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjZmZmZmZmLCAjZmRmOGZmKTtcbiAgICAgICAgICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIC8vIG92ZXJmbG93OiBhdXRvO1xuICAgICAgICAgICAgLy8gLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSg4OSUsIDQ5JSk7XG4gICAgICAgICAgICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1OSUsIDI2JSk7XG4gICAgICAgICAgICAvLyB3aWR0aDogNDYlO1xuICAgICAgICAgICAgLy8gaGVpZ2h0OiAxNyU7XG4gICAgICAgICAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAvLyBwYWRkaW5nOiAzJTtcbiAgICAgICAgICAgIC8vIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgQG1lZGlhIGFsbCBhbmQgKG1heC13aWR0aDogNjAwcHgpIFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIC52aWRlb3tcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDE2NCU7XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMzI3cHg7XG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAtMTE4cHg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIC5tZXNzYWdlMntcbiAgICAgICAgICAgICAgICAgICAgLy9iYWNrZ3JvdW5kLWltYWdlOiB3aGl0ZTtcbiAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCM3NTc5ZmYsI2IyMjRlZiA3MCUpO1xuICAgICAgICAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICNmZmZmZmYsICNmZGY4ZmYpO1xuICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgICAgICAvKiBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgKi9cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBAbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkgXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAudmlkZW97XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMzQ5cHg7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLm1lc3NhZ2Uye1xuICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheTpub25lO1xuICAgICAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgICAgICAgICAgICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIC8vIG92ZXJmbG93OiBhdXRvO1xuICAgICAgICAgICAgICAgIC8vIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUoODklLCA0OSUpO1xuICAgICAgICAgICAgICAgIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlKDU5JSwgMjYlKTtcbiAgICAgICAgICAgICAgICAvLyB3aWR0aDogNDYlO1xuICAgICAgICAgICAgICAgIC8vIGhlaWdodDogMTclO1xuICAgICAgICAgICAgICAgIC8vIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgICAvLyBwYWRkaW5nOiAzJTtcbiAgICAgICAgICAgICAgICAvLyBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4vKjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWRhcmspLCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSkpO1xuICAgIH1cbn1cblxuLnBheiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDEwO1xufVxuKi9cbiIsIi5uZXctYmFja2dyb3VuZC1jb2xvciB7XG4gIC0tYmFja2dyb3VuZDogI2ZmYjAxYTsgfVxuXG5pb24tdGl0bGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5zdWJtaXQtYnRuIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZiMDFhO1xuICAtLWNvbG9yOiAjMTYxYjEyO1xuICBwb3NpdGlvbjogY2VudGVyOyB9XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC5tZXNzYWdlIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMjAwZGVnLCAjNzU3OWZmLCAjYjIyNGVmIDcwJSk7XG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjZmZmZmZmLCAjZmRmOGZmKTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCUsIDQ5JSk7XG4gICAgd2lkdGg6IDg2JTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcGFkZGluZzogMSU7XG4gICAgYm94LXNoYWRvdzogMHB4IDE5cHggMzhweCByZ2JhKDAsIDAsIDAsIDAuMyksIDAgMTVweCAxMnB4IHJnYmEoMCwgMCwgMCwgMC4yMik7IH0gfVxuXG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xuICAubWVzc2FnZSB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDIwMGRlZywgIzc1NzlmZiwgI2IyMjRlZiA3MCUpO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgI2ZmZmZmZiwgI2ZkZjhmZik7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDg5JSwgMTglKTtcbiAgICB3aWR0aDogMzUlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAzJTtcbiAgICBib3gtc2hhZG93OiAwcHggMTlweCAzOHB4IHJnYmEoMCwgMCwgMCwgMC4zKSwgMCAxNXB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjIyKTsgfSB9XG5cbiNjb3ZlciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiA1MiU7XG4gIGxlZnQ6IDA7XG4gIHotaW5kZXg6IDU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDUwMCU7XG4gIGN1cnNvcjogd2FpdDsgfVxuXG4ubWVzc2FnZXMge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKDI4NSUsIDIwMCUpO1xuICB3aWR0aDogMTUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDMlOyB9XG5cbkBtZWRpYSBhbGwgYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XG4gIC52aWRlbyB7XG4gICAgd2lkdGg6IDE2NCU7XG4gICAgaGVpZ2h0OiAzMjdweDtcbiAgICBtYXJnaW4tbGVmdDogLTExOHB4OyB9XG4gIC5tZXNzYWdlMSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZSg0JSwgMjUlKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMSUsIDY0JSk7XG4gICAgd2lkdGg6IDEwMiU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDElO1xuICAgIC8qIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpOyAqLyB9IH1cblxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNjAwcHgpIHtcbiAgLnZpZGVvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDM0OXB4OyB9XG4gIC5tZXNzYWdlMSB7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogd2hpdGU7IH0gfVxuXG5AbWVkaWEgYWxsIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xuICAudmlkZW8ge1xuICAgIHdpZHRoOiAxNjQlO1xuICAgIGhlaWdodDogMzI3cHg7XG4gICAgbWFyZ2luLWxlZnQ6IC0xMThweDsgfVxuICAubWVzc2FnZTIge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIC8qIGJveC1zaGFkb3c6IDBweCAxOXB4IDM4cHggcmdiYSgwLCAwLCAwLCAwLjMpLCAwIDE1cHggMTJweCByZ2JhKDAsIDAsIDAsIDAuMjIpOyAqLyB9IH1cblxuQG1lZGlhIGFsbCBhbmQgKG1pbi13aWR0aDogNjAwcHgpIHtcbiAgLnZpZGVvIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDM0OXB4OyB9XG4gIC5tZXNzYWdlMiB7XG4gICAgZGlzcGxheTogbm9uZTsgfSB9XG5cbi8qOmhvc3Qge1xuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItZGFyayksIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KSk7XG4gICAgfVxufVxuXG4ucGF6IHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTA7XG59XG4qL1xuIl19 */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Services/authentication.service */ "./src/app/Services/authentication.service.ts");
/* harmony import */ var _Services_Login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Services/Login.service */ "./src/app/Services/Login.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Services/data.service */ "./src/app/Services/data.service.ts");
/* harmony import */ var _Services_encr_decr_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Services/encr-decr-service.service */ "./src/app/Services/encr-decr-service.service.ts");







var LoginPage = /** @class */ (function () {
    function LoginPage(authentication, loginservice, EncrDecr, events, data) {
        this.authentication = authentication;
        this.loginservice = loginservice;
        this.EncrDecr = EncrDecr;
        this.events = events;
        this.data = data;
        this.show = false;
        this.type = 'password';
        this.showPass = false;
        this.events.publish('PageName', 'login');
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.doAsyncTask = function (username, password) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.loginservice.getUserDetails(username, password)
                    .subscribe(function (data) {
                    _this.User = data;
                    console.log('userdate', data);
                    localStorage.setItem('Profile_Type', data.Profile_Type);
                    localStorage.setItem('Profile_Type', data.Profile_Type);
                    localStorage.setItem('Locale', data.Locale);
                    localStorage.setItem('DateFormat', data.DateFormat);
                    localStorage.setItem('Currency', data.Currency);
                    localStorage.setItem('TimeZone', data.TimeZone);
                    localStorage.setItem('HourFormat', data.HourFormat.toString());
                    if (_this.User.Error != null) {
                        _this.show = false;
                        _this.error = _this.User.Error;
                    }
                    else {
                        _this.error = null;
                        _this.data.storage = {
                            "firstname": "Nic",
                            "lastname": "Raboy",
                            "address": {
                                "city": "San Francisco",
                                "state": "California"
                            }
                        };
                        _this.doAsyncLogin(username, password).then(function () { return _this.show = true; }, function () { return console.log("Task Errored!"); });
                        //this.authentication.login(username, password);
                        // this.show = false;
                    }
                }, function (error) {
                    _this.show = false;
                    _this.error = error;
                });
                resolve();
            }, 1000);
        });
        this.show = false;
        return promise;
    };
    LoginPage.prototype.doAsyncLogin = function (username, password) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.authentication.login(username, password);
                resolve();
            }, 1000);
        });
        // this.show=false;
        return promise;
    };
    LoginPage.prototype.loginUser = function (e) {
        var _this = this;
        e.preventDefault();
        console.log(e);
        var token;
        // var username = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[0].value);
        // var password = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[1].value);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
        this.show = true;
        this.doAsyncTask(username, password).then(function () { return _this.show = true; }, function () { return console.log("Task Errored!"); });
        // this.loginservice.getUserDetails(username, password)
        //     .subscribe(data => {
        //     this.User = data;
        //     localStorage.setItem('Profile_Type',data.Profile_Type);
        //     localStorage.setItem('Profile_Type',data.Profile_Type);
        //     localStorage.setItem('Locale',data.Locale);
        //     localStorage.setItem('DateFormat',data.DateFormat);
        //     localStorage.setItem('Currency',data.Currency);
        //     localStorage.setItem('TimeZone',data.TimeZone);
        //     if (this.User.Error != null) {
        //         this.show = false;
        //         this.error = this.User.Error;
        //     }
        //     else {
        //         this.error = null;
        //         this.data.storage = {
        //             "firstname": "Nic",
        //             "lastname": "Raboy",
        //             "address": {
        //                 "city": "San Francisco",
        //                 "state": "California"
        //             }
        //         }
        //         this.doAsyncTask(username,password).then(
        //             () => this.show=true,
        //             () => console.log("Task Errored!"),
        //           );
        //         //this.authentication.login(username, password);
        //         // this.show = false;
        //     }
        //     },
        //     error => {
        //         this.show = false;
        //         this.error = error;
        //     })
    };
    LoginPage.prototype.showPassword = function () {
        this.showPass = !this.showPass;
        if (this.showPass) {
            this.type = 'text';
        }
        else {
            this.type = 'password';
        }
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_Services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"], _Services_Login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _Services_encr_decr_service_service__WEBPACK_IMPORTED_MODULE_6__["EncrDecrServiceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Events"],
            _Services_data_service__WEBPACK_IMPORTED_MODULE_5__["Data"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map