(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["registration-registration-module"],{

/***/ "./src/app/registration/registration.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/registration/registration.module.ts ***!
  \*****************************************************/
/*! exports provided: RegistrationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPageModule", function() { return RegistrationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _registration_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./registration.page */ "./src/app/registration/registration.page.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/esm5/stepper.es5.js");





//import { IonSimpleWizard } from '../ion-simple-conditional-wizard/ion-simple-wizard.component';
//import { IonSimpleWizardStep } from '../ion-simple-conditional-wizard/ion-simple-wizard.step.component';




var routes = [
    {
        path: '',
        component: _registration_page__WEBPACK_IMPORTED_MODULE_6__["RegistrationPage"]
    }
];
var RegistrationPageModule = /** @class */ (function () {
    function RegistrationPageModule() {
    }
    RegistrationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_8__["CdkStepperModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatStepperModule"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_registration_page__WEBPACK_IMPORTED_MODULE_6__["RegistrationPage"]
                //, IonSimpleWizard,
                //IonSimpleWizardStep
            ]
        })
    ], RegistrationPageModule);
    return RegistrationPageModule;
}());



/***/ }),

/***/ "./src/app/registration/registration.page.html":
/*!*****************************************************!*\
  !*** ./src/app/registration/registration.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>registration</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n    <ion-slides #slides>\r\n        <ion-slide>    \r\n            <form name=\"form\" (ngSubmit)=\"f.form.valid && onSubmit(f.value,60)\" #f=\"ngForm\" novalidate>\r\n                <ion-item>\r\n                    <ion-label position=\"floating\"> Add Driver First Name </ion-label>\r\n                    <ion-input type=\"string\" name=\"DriverName\" #DriverName217=\"ngModel\" required ngModel pattern=\"\"></ion-input>\r\n                </ion-item>\r\n                <div *ngIf=\"DriverName217?.invalid && (DriverName217?.dirty || DriverName217?.touched)\"\r\n                     class=\"error\">\r\n                    <div *ngIf=\"DriverName217?.errors?.required\">\r\n                        Add Driver First Name is required.\r\n                    </div>\r\n                    <div *ngIf=\"DriverName217?.errors?.pattern\">\r\n                        Add Driver First Name not valid.\r\n                    </div>\r\n\r\n                </div>\r\n            \r\n                <ion-button type=\"button\" float-right class=\"submit-btn\" [disabled]=\"!f.form.valid\" (click)=\"Next()\">Next</ion-button>\r\n            </form>\r\n        </ion-slide>\r\n\r\n        <ion-slide>\r\n            <form name=\"form\" (ngSubmit)=\"f1.form.valid && onSubmit(f1.value,60)\" #f1=\"ngForm\" novalidate>\r\n                <ion-item>\r\n                    <ion-label position=\"floating\"> Add Driver First Name </ion-label>\r\n                    <ion-input type=\"string\" name=\"DriverName\" #DriverNam17=\"ngModel\" required ngModel pattern=\"\"></ion-input>\r\n                </ion-item>\r\n                <div *ngIf=\"DriverName17?.invalid && (DriverName17?.dirty || DriverName17?.touched)\"\r\n                     class=\"error\">\r\n                    <div *ngIf=\"DriverName17?.errors?.required\">\r\n                        Add Driver First Name is required.\r\n                    </div>\r\n                    <div *ngIf=\"DriverName17?.errors?.pattern\">\r\n                        Add Driver First Name not valid.\r\n                    </div>\r\n\r\n                </div>                           \r\n                <ion-button type=\"button\" float-left class=\"submit-btn\" (click)=\"Prev()\">Previous</ion-button>\r\n                <ion-button type=\"button\" float-right class=\"submit-btn\" [disabled]=\"!f1.form.valid\" (click)=\"Next()\">Next</ion-button>\r\n            </form>\r\n           \r\n        </ion-slide>\r\n    </ion-slides>\r\n    <!--<mat-horizontal-stepper [linear]=\"isLinear\" #stepper>\r\n        <mat-step [stepControl]=\"firstFormGroup\">\r\n            <form [formGroup]=\"firstFormGroup\">\r\n                <ng-template matStepLabel>Enter your name</ng-template>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\r\n                </mat-form-field>\r\n                <div>\r\n                    <button mat-button matStepperNext>Next</button>\r\n                </div>\r\n            </form>\r\n        </mat-step>\r\n        <mat-step [stepControl]=\"secondFormGroup\">\r\n            <form [formGroup]=\"secondFormGroup\">\r\n                <ng-template matStepLabel>Enter your address</ng-template>\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\r\n                </mat-form-field>\r\n                <div>\r\n                    <button mat-button matStepperPrevious>Back</button>\r\n                    <button mat-button matStepperNext>Next</button>\r\n                </div>\r\n            </form>\r\n        </mat-step>\r\n        <mat-step>\r\n            <ng-template matStepLabel>Done</ng-template>\r\n            Details taken.\r\n            <div>\r\n                <button mat-button matStepperPrevious>Back</button>\r\n                <button mat-button (click)=\"stepper.reset()\">Reset</button>\r\n            </div>\r\n        </mat-step>\r\n    </mat-horizontal-stepper>-->\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/registration/registration.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/registration/registration.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdHJhdGlvbi9yZWdpc3RyYXRpb24ucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/registration/registration.page.ts":
/*!***************************************************!*\
  !*** ./src/app/registration/registration.page.ts ***!
  \***************************************************/
/*! exports provided: RegistrationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistrationPage", function() { return RegistrationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(navCtrl, _formBuilder) {
        this.navCtrl = navCtrl;
        this._formBuilder = _formBuilder;
        this.title = 'materialApp';
    }
    RegistrationPage.prototype.selectChange = function (e) {
        console.log(e);
    };
    RegistrationPage.prototype.Next = function () {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    };
    RegistrationPage.prototype.Prev = function () {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    };
    RegistrationPage.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    RegistrationPage.prototype.ngAfterViewInit = function () {
        this.slides.lockSwipes(true);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])
    ], RegistrationPage.prototype, "slides", void 0);
    RegistrationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-registration',
            template: __webpack_require__(/*! ./registration.page.html */ "./src/app/registration/registration.page.html"),
            styles: [__webpack_require__(/*! ./registration.page.scss */ "./src/app/registration/registration.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], RegistrationPage);
    return RegistrationPage;
}());



/***/ })

}]);
//# sourceMappingURL=registration-registration-module.js.map