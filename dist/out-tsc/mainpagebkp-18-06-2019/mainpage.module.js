import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { IonicModule } from '@ionic/angular';
import { routes } from './mainpage.routes';
import { IonicSelectableModule } from 'ionic-selectable';
import { Menu7Page } from './menu7/menu7.component';
import { Menu2Page } from './menu2/menu2.component';
import { Menu8Page } from './menu8/menu8.component';
import { Menu3Page } from './menu3/menu3.component';
import { Menu10Page } from './menu10/menu10.component';
import { Menu11Page } from './menu11/menu11.component';
import { Menu14Page } from './menu14/menu14.component';
import { MatTableModule, MatSortModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatPaginatorModule, MatCardModule, MatProgressSpinnerModule, MatIconModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { IonicStorageModule } from '@ionic/storage';
//import { AgGridModule } from 'ag-grid-angular';
import { MainpagePage } from './mainpage.page';
import { Data } from '../Services/data.service';
var MainpagePageModule = /** @class */ (function () {
    function MainpagePageModule() {
    }
    MainpagePageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                DataTablesModule,
                IonicSelectableModule,
                MatTableModule, MatSortModule, MatButtonModule,
                MatFormFieldModule,
                MatInputModule,
                MatPaginatorModule,
                MatRippleModule,
                MatCardModule,
                MatProgressSpinnerModule,
                MatIconModule,
                CdkTableModule,
                ReactiveFormsModule,
                IonicStorageModule.forRoot()
                //AgGridModule.withComponents([])
            ],
            providers: [Data],
            declarations: [MainpagePage, Menu7Page, Menu2Page,
                Menu8Page, Menu3Page, Menu10Page, Menu11Page, Menu14Page],
            entryComponents: []
        })
    ], MainpagePageModule);
    return MainpagePageModule;
}());
export { MainpagePageModule };
//# sourceMappingURL=mainpage.module.js.map