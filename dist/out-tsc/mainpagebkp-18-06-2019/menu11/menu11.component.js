import { __decorate, __metadata } from "tslib";
import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Subject } from 'rxjs';
import { MainPageService } from '../../Services/MainPage.service';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { saveAs } from 'file-saver';
import { NgZone } from "@angular/core"; //added
import { Storage } from '@ionic/storage'; //added
import { IonSlides } from '@ionic/angular';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
var Menu11Page = /** @class */ (function () {
    function Menu11Page(mainpageservice, router, toastController, //Added
    activateRouter, zone, //Added
    events, storage //Added
    ) {
        var _this = this;
        this.mainpageservice = mainpageservice;
        this.router = router;
        this.toastController = toastController;
        this.activateRouter = activateRouter;
        this.zone = zone;
        this.events = events;
        this.storage = storage;
        this.Sys_Menu_ID = 11;
        this.dtOptions = {};
        this.dtTrigger = new Subject();
        this.datatableparam = {};
        this.WizardFormValue = {};
        this.MyDataSourceArray = [];
        this.isExpansionDetailRow = function (i, row) { return row.hasOwnProperty('detailRow'); };
        this.isLoadingResults = true;
        this.displayedColumnArray = [];
        this.displayedColumns = [];
        this.show = false;
        /////////////////Convert Image to blob////////////////////
        this.base64textString = "";
        events.subscribe('navigationExtras', function (name) {
            _this.show = true;
            // alert('change'+name);
            var data;
            data = JSON.stringify(name);
            data = '{Filter:' + data + '}';
            alert(data);
            sessionStorage.setItem('ses-data', data);
            console.log("filterval" + data);
            _this.mainpageservice.GetModulesWithFilter(_this.Sys_Menu_ID, _this.UserName, data).subscribe(function (data) {
                console.log("Moduleapi", data);
                _this.Module = data;
                console.log(data);
                _this.show = false;
            });
        });
    }
    Menu11Page.prototype.ngOnInit = function () {
        var _this = this;
        this.show = true;
        var data;
        this.UserName = localStorage.getItem('username');
        this.dtOptions = this.mainpageservice.buildDtOptions();
        this.mainpageservice.getPageMenuDetails(this.Sys_Menu_ID, this.UserName).subscribe(function (filter) {
            console.log('filter', filter);
            _this.PageMenuDetails = filter;
            _this.show = false;
        });
        data = sessionStorage.getItem('ses-data');
        console.log("filterval" + data);
        this.activateRouter.queryParams.subscribe(function (params) {
            _this.show = true;
            // alert('change'+name);
            var data;
            data = JSON.stringify(params);
            data = '{Filter:' + data + '}';
            sessionStorage.setItem('ses-data', data);
            console.log("filterval" + data);
            _this.mainpageservice.GetModulesWithFilter(_this.Sys_Menu_ID, _this.UserName, data).subscribe(function (data) {
                console.log("Moduleapi", data);
                _this.Module = data;
                _this.show = false;
                //debugger;
                _this.MyDataSource = new MatTableDataSource();
                var rows = [];
                var count = 0;
                //res.forEach(element => rows.push(element, { detailRow: true, element }));
                //console.log(rows);
                _this.Module[0].moduleList.forEach(function (val) {
                    if (val.DisplayType === '') {
                        _this.displayedColumns = [];
                        _this.MyDataSource = new MatTableDataSource();
                        _this.MyDataSource.data = val.moduleData;
                        //for (let key in val.moduleData[0]) {
                        //    this.displayedColumns.push((key));
                        //}
                        val.moduleDetails.forEach(function (val) {
                            _this.displayedColumns.push((val.ColumnName));
                        });
                        _this.displayedColumnArray.push(_this.displayedColumns);
                        //val.moduleData.forEach(element => rows.push(element, { detailRow: true, element }));
                        //    console.log(rows);
                        _this.MyDataSourceArray.push(_this.MyDataSource);
                        _this.isLoadingResults = false;
                        count++;
                    }
                }); //this.dataSource = new MatTableDataSource(this.Module);
                // this.dtTrigger.next();
            });
        });
        //this.dtOptions = {
        //    pagingType: 'full_numbers',
        //    pageLength: 10,
        //    serverSide: true,
        //    processing: true,
        //    //to restrict from ordering the data
        //    order: [],
        //    columnDefs: [{ orderable: false, targets: [0] }],
        //    ajax: (dataTablesParameters: any, callback) => {
        //        this.mainpageservice.GetPaginatedData(this.Sys_Menu_ID, this.UserName, dataTablesParameters, null).subscribe(resp => {
        //            sessionStorage.setItem('datatableparameter', JSON.stringify(dataTablesParameters));
        //            this.dt_Module = resp.data;
        //            console.log("ModuleData" + resp.data);
        //            this.show = false;
        //            callback({
        //                recordsTotal: resp.recordsTotal,
        //                recordsFiltered: resp.recordsFiltered,
        //                data: []
        //            });
        //        });
        //    },
        //    responsive: true
        //};
        //this.dtTrigger.next();
    };
    //To submit Form
    Menu11Page.prototype.onSubmit = function (form, ID) {
        var _this = this;
        var that = this;
        var Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, this.UserName).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
        });
    };
    Menu11Page.prototype.Savedatatable = function (form, ID) {
        var _this = this;
        var that = this;
        var Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        //Form Without FileUpload
        this.mainpageservice.SaveDatatable(ID, form, this.UserName).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
        });
    };
    Menu11Page.prototype.onSubmitandload = function (form, ID) {
        var _this = this;
        var that = this;
        var Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        console.log(form);
        var data = sessionStorage.getItem('ses-data');
        console.log(data);
        this.mainpageservice.Savedata(ID, form, this.UserName).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            _this.show = false;
            toast.then(function (toast) { return toast.present(); });
        });
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, data).subscribe(function (data) {
            // alert(this.UserName);
            console.log("Moduleapi", data);
            _this.Module = data;
            console.log(data);
            _this.show = false;
        });
    };
    Menu11Page.prototype.onSubmitFilter = function (formdata) {
        var _this = this;
        console.log('you submitted value in Filter:', formdata);
        var data;
        data = JSON.stringify(formdata);
        data = '{Filter:' + data + '}';
        console.log(data);
        this.UserName = localStorage.getItem('username');
        sessionStorage.setItem('filterdata', data);
        this.datatableparam = sessionStorage.getItem('datatableparameter');
        console.log(this.datatableparam);
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, data).subscribe(function (filter) {
            console.log('filteredData', filter);
            _this.dt_Module = filter;
            _this.dtElement.dtInstance.then(function (dtInstance) {
                // Destroy the table first
                // Call the dtTrigger to rerender again
                dtInstance.destroy();
                _this.dtTrigger.next();
            });
        });
    };
    Menu11Page.prototype.downloadreport = function () {
        //alert(menuid);
        this.show = true;
        var data;
        this.UserName = localStorage.getItem('username');
        data = sessionStorage.getItem('filterdata');
        this.mainpageservice.exporttoexcel(this.Sys_Menu_ID, this.UserName, data).subscribe(function (resp) {
            console.log(resp);
            //  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            var blob = new Blob([resp], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            var filename = 'download_report.xlsx';
            saveAs(blob, filename);
        });
        this.show = false;
    };
    Menu11Page.prototype.navigate = function (i, LinkedMenu) {
        var qm = { "rowval": i };
        var navigationExtras = {
            queryParams: {
                "rowval": i
            }
        };
        this.events.publish('navigationExtras', qm);
        this.router.navigate(["/menu/first/tabs/" + LinkedMenu], navigationExtras);
        // this.router.navigate(["/menu/first/tabs/GotoForm"], navigationExtras);
    };
    Menu11Page.prototype.ionViewDidLoad = function () { };
    Menu11Page.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
        var paginatorArrya = this.paginatorList.toArray();
        var sortarray = this.sortList.toArray();
        var count = 0;
        this.MyDataSourceArray.forEach(function (item) {
            debugger;
            item.paginator = paginatorArrya[count];
            item.sort = sortarray[count];
            count++;
        });
        // this.slides.lockSwipes(true);
    };
    Menu11Page.prototype.ngOnDestroy = function () {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    };
    Menu11Page.prototype.onWizardSubmit = function (ID) {
        var _this = this;
        debugger;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        var form = JSON.stringify(this.WizardFormValue);
        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, null).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
        });
    };
    Menu11Page.prototype.Next = function (ID, wizardFormdata) {
        debugger;
        this.WizardFormValue = Object.assign(this.WizardFormValue, wizardFormdata);
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
        /*this.content.scrollToTop();*/
    };
    Menu11Page.prototype.Prev = function () {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
        /*this.content.scrollToTop();*/
    };
    Menu11Page.prototype.handleFileInput = function (e) {
        var files = e.target.files;
        var file = files[0];
        if (files && file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    };
    Menu11Page.prototype._handleReaderLoaded = function (readerEvt) {
        var binaryString = readerEvt.target.result;
        this.id = Math.random();
        sessionStorage.setItem('id', this.id);
        this.base64textString = btoa(binaryString);
        console.log(btoa(binaryString));
        this.storage.set('sampler-' + this.id, btoa(binaryString));
        var FileType = {
            name: this.FileName,
            FileData: this.b64toBlob(btoa(binaryString), '', '')
        };
        console.log(btoa(binaryString));
        this.storage.set('FileType-' + this.id, FileType);
        var toast = this.toastController.create({
            message: "Your image is stored. Click on Submit to save the form.",
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Ok',
        });
        toast.then(function (toast) { return toast.present(); });
        console.log('blob data while passing to savedata1' + JSON.stringify(FileType));
    };
    Menu11Page.prototype.b64toBlob = function (b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    Menu11Page.prototype.RenderDataTable = function () {
        var _this = this;
        this.show = true;
        this.UserName = localStorage.getItem('username');
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, undefined)
            .subscribe(function (res) {
            debugger;
            _this.MyDataSource = new MatTableDataSource();
            _this.Module = res;
            // const rows = [];
            var count = 0;
            //res.forEach(element => rows.push(element, { detailRow: true, element }));
            //console.log(rows);
            _this.Module[0].moduleList.forEach(function (val) {
                //if (val.DisplayType === '') {
                _this.MyDataSource = new MatTableDataSource();
                _this.MyDataSource.data = val.moduleData;
                //val.moduleData.forEach(element => rows.push(element, { detailRow: true, element }));
                //    console.log(rows);
                _this.MyDataSourceArray.push(_this.MyDataSource);
                _this.isLoadingResults = false;
                count++;
                //}
                //else {
                //    alert(val.DisplayType);
                //}
            });
        }, function (error) {
            console.log('There was an error while retrieving Posts !!!' + error);
        });
    };
    Menu11Page.prototype.applyFilterForTable = function (filterValue, index) {
        this.MyDataSourceArray[index].filter = filterValue.trim().toLowerCase();
        if (this.MyDataSourceArray[index].paginator) {
            this.MyDataSourceArray[index].paginator.firstPage();
        }
    };
    var _a;
    __decorate([
        ViewChild(IonSlides),
        __metadata("design:type", IonSlides)
    ], Menu11Page.prototype, "slides", void 0);
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], Menu11Page.prototype, "dtElement", void 0);
    __decorate([
        ViewChildren(MatPaginator),
        __metadata("design:type", QueryList)
    ], Menu11Page.prototype, "paginatorList", void 0);
    __decorate([
        ViewChildren(MatSort),
        __metadata("design:type", QueryList)
    ], Menu11Page.prototype, "sortList", void 0);
    Menu11Page = __decorate([
        Component({
            selector: 'app-menu11',
            templateUrl: './menu11.component.html',
            styleUrls: ['./menu11.component.scss'],
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof MainPageService !== "undefined" && MainPageService) === "function" ? _a : Object, Router,
            ToastController,
            ActivatedRoute,
            NgZone,
            Events,
            Storage //Added
        ])
    ], Menu11Page);
    return Menu11Page;
}());
export { Menu11Page };
//# sourceMappingURL=menu11.component.js.map