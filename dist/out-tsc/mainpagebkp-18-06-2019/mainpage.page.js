import { __decorate, __metadata } from "tslib";
import { Component, Compiler } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from '../Services/menu.service';
import { Router } from '@angular/router';
//import { Page1Component } from './page1/page1.component';
import { Events } from '@ionic/angular';
var MainpagePage = /** @class */ (function () {
    //MenuId: number=1;
    function MainpagePage(activatedroute, menuservice, router, events, _compiler) {
        this.activatedroute = activatedroute;
        this.menuservice = menuservice;
        this.router = router;
        this.events = events;
        this._compiler = _compiler;
        console.log('beforactive', activatedroute.routeConfig);
        //this.menuservice.getJSON().subscribe(data => {
        //    let jsondata= JSON.parse(data);
        //    let routes = <Routes>[];
        //    jsondata.forEach(route => {
        //        let ro1: Route = {};
        //        ro1.path = route.path;
        //        ro1.component = route.component as Type<Menu1Page>;
        //        ro1.loadChildren = './amainpage/sample/sample.module#SamplePageModule';
        //        console.log('myroutes', ro1);
        //        routes.unshift(ro1);
        //    });
        //    activatedroute.routeConfig.children = routes;
        //    console.log('activeroutes', activatedroute.routeConfig);
        //    this.router.resetConfig(config);  
        //});
        //activatedroute.routeConfig.children = [
        //    {
        //        path: "page1", component: Page1Component 
        //    },
        //    {
        //        path: "1", component: Menu1Page 
        //    }
        //];
        //this.router.config.unshift(
        //    { path: 'menu/first/tabs/page1', component: Page1Component }
        //);
    }
    MainpagePage.prototype.ngOnInit = function () {
        var _this = this;
        this.UserName = localStorage.getItem('username');
        this.activatedroute.queryParams.subscribe(function (params) {
            _this.ID = params['id'];
            _this.menuservice.getmenus(_this.ID, _this.UserName)
                .subscribe(function (data) {
                _this.SubMenu = data;
                console.log('PageName', _this.SubMenu[0].MenuName);
                _this.events.publish('PageName', _this.SubMenu[0].MenuName);
                //this.MenuID = this.SubMenu[0].ID;
            });
        });
    };
    var _a;
    MainpagePage = __decorate([
        Component({
            selector: 'app-mainpage',
            templateUrl: './mainpage.page.html',
            styleUrls: ['./mainpage.page.scss'],
        }),
        __metadata("design:paramtypes", [ActivatedRoute, typeof (_a = typeof MenuService !== "undefined" && MenuService) === "function" ? _a : Object, Router, Events, Compiler])
    ], MainpagePage);
    return MainpagePage;
}());
export { MainpagePage };
//# sourceMappingURL=mainpage.page.js.map