import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { HeaderPage } from './header.page';
var routes = [
    {
        path: '',
        component: HeaderPage
    }
];
var HeaderPageModule = /** @class */ (function () {
    function HeaderPageModule() {
    }
    HeaderPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [HeaderPage],
            exports: [HeaderPage]
        })
    ], HeaderPageModule);
    return HeaderPageModule;
}());
export { HeaderPageModule };
//# sourceMappingURL=header.module.js.map