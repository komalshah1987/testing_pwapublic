import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { IonicModule } from '@ionic/angular';
import { routes } from './mainpage.routes';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { MdePopoverModule } from '@material-extended/mde';
import { MatToolbarModule } from '@angular/material';
import { MatTableModule, MatSortModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule, MatPaginatorModule, MatCardModule, MatProgressSpinnerModule, MatIconModule, MatExpansionModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { IonicStorageModule } from '@ionic/storage';
//import { AgGridModule } from 'ag-grid-angular';
import { MainpagePage } from './mainpage.page';
import { Data } from '../Services/data.service';
import { Menu1Page } from './menu1/menu1.component';
import { Menu2Page } from './menu2/menu2.component';
import { Menu4Page } from './menu4/menu4.component';
import { Menu5Page } from './menu5/menu5.component';
import { Menu6Page } from './menu6/menu6.component';
import { Menu7Page } from './menu7/menu7.component';
import { Menu1007Page } from './menu1007/menu1007.component';
import { Menu1009Page } from './menu1009/menu1009.component';
import { Menu1010Page } from './menu1010/menu1010.component';
import { Menu1011Page } from './menu1011/menu1011.component';
import { Menu1012Page } from './menu1012/menu1012.component';
import { Menu1013Page } from './menu1013/menu1013.component';
import { Menu1014Page } from './menu1014/menu1014.component';
import { Menu1015Page } from './menu1015/menu1015.component';
import { Menu1016Page } from './menu1016/menu1016.component';
import { Menu1017Page } from './menu1017/menu1017.component';
var MainpagePageModule = /** @class */ (function () {
    function MainpagePageModule() {
    }
    MainpagePageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes),
                DataTablesModule,
                IonicSelectableModule,
                MatTableModule, MatSortModule, MatButtonModule,
                MatFormFieldModule,
                MatInputModule,
                MatPaginatorModule,
                MatRippleModule,
                MatCardModule,
                MatProgressSpinnerModule,
                MatIconModule,
                CdkTableModule,
                ReactiveFormsModule,
                IonicStorageModule.forRoot(),
                MatExpansionModule,
                NgQrScannerModule,
                MdePopoverModule,
                MatToolbarModule
                //AgGridModule.withComponents([])
            ],
            providers: [Data],
            declarations: [MainpagePage, Menu1Page, Menu2Page, Menu4Page, Menu5Page, Menu6Page, Menu7Page, Menu1007Page,
                Menu1009Page, Menu1010Page, Menu1011Page, Menu1012Page, Menu1013Page, Menu1014Page, Menu1015Page, Menu1016Page,
                Menu1017Page],
            entryComponents: []
        })
    ], MainpagePageModule);
    return MainpagePageModule;
}());
export { MainpagePageModule };
//# sourceMappingURL=mainpage.module.js.map