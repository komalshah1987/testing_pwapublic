import { __decorate, __metadata } from "tslib";
import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { Subject } from 'rxjs';
import { MainPageService } from '../../Services/MainPage.service';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { saveAs } from 'file-saver';
import { NgZone } from "@angular/core"; //added
import { Storage } from '@ionic/storage'; //added
import { IonSlides } from '@ionic/angular';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
var Menu7Page = /** @class */ (function () {
    function Menu7Page(mainpageservice, router, toastController, //Added
    activateRouter, zone, //Added
    events, storage //Added
    ) {
        var _this = this;
        this.mainpageservice = mainpageservice;
        this.router = router;
        this.toastController = toastController;
        this.activateRouter = activateRouter;
        this.zone = zone;
        this.events = events;
        this.storage = storage;
        this.Sys_Menu_ID = 7;
        this.dtOptions = {};
        this.dtTrigger = new Subject();
        this.datatableparam = {};
        this.WizardFormValue = {};
        this.MyDataSourceArray = [];
        this.isExpansionDetailRow = function (i, row) { return row.hasOwnProperty('detailRow'); };
        this.isLoadingResults = true;
        this.displayedColumnArray = [];
        this.displayedColumns = [];
        this.show = false;
        /////////////////Convert Image to blob////////////////////
        //private base64textString: String = "";
        //handleFileInput(e) {
        //    var files = e.target.files;
        //    var file = files[0];
        //    if (files && file) {
        //        var reader = new FileReader();
        //        reader.onload = this._handleReaderLoaded.bind(this);
        //        reader.readAsBinaryString(file);
        //    }
        //}
        this.fileToUpload = null;
        events.subscribe('navigationExtras', function (name) {
            _this.show = true;
            // alert('change'+name);
            var data;
            data = JSON.stringify(name);
            data = '{Filter:' + data + '}';
            // alert(data);
            _this.mainpageservice.GetModulesWithFilter(_this.Sys_Menu_ID, _this.UserName, data).subscribe(function (data) {
                console.log("Moduleapi", data);
                _this.Module = data;
                console.log(data);
                _this.show = false;
            });
        });
    }
    Menu7Page.prototype.sortingDataAccessor = function (item, property) {
        debugger;
        if (property.includes('.')) {
            return property.split('.')
                .reduce(function (object, key) { return object[key]; }, item);
        }
        return item[property].Value;
    }; //sorting nested object array
    Menu7Page.prototype.nestedFilterCheck = function (search, data, key) {
        if (typeof data[key] === 'object') {
            for (var k in data[key]) {
                if (data[key][k] !== null) {
                    search = this.nestedFilterCheck(search, data[key], k);
                }
            }
        }
        else {
            search += data[key];
        }
        return search;
    }; //searching nested object array
    Menu7Page.prototype.ngOnInit = function () {
        var _this = this;
        this.show = true;
        this.UserName = localStorage.getItem('username');
        this.dtOptions = this.mainpageservice.buildDtOptions();
        this.mainpageservice.getPageMenuDetails(this.Sys_Menu_ID, this.UserName).subscribe(function (filter) {
            console.log('filter', filter);
            _this.PageMenuDetails = filter;
            _this.show = false;
        });
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, undefined).subscribe(function (data) {
            console.log("Moduleapi", data);
            _this.Module = data;
            _this.show = false;
            //debugger;
            _this.MyDataSource = new MatTableDataSource();
            var rows = [];
            var count = 0;
            //res.forEach(element => rows.push(element, { detailRow: true, element }));
            //console.log(rows);
            _this.Module[0].moduleList.forEach(function (val) {
                if (val.DisplayType === '') {
                    _this.displayedColumns = [];
                    _this.MyDataSource = new MatTableDataSource();
                    var dataArray_1 = [];
                    dataArray_1 = val.ModuleDataWithRule;
                    var resultArray = Object.keys(dataArray_1).map(function (index) {
                        var data = dataArray_1[index];
                        return data;
                    });
                    _this.MyDataSource.filterPredicate = function (data, filter) {
                        var accumulator = function (currentTerm, key) {
                            return _this.nestedFilterCheck(currentTerm, data, key);
                        };
                        var dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
                        // Transform the filter by converting it to lowercase and removing whitespace.
                        var transformedFilter = filter.trim().toLowerCase();
                        return dataStr.indexOf(transformedFilter) !== -1;
                    };
                    _this.MyDataSource.sortingDataAccessor = _this.sortingDataAccessor;
                    _this.MyDataSource.sort = _this.sort;
                    _this.MyDataSource.data = resultArray;
                    //for (let key in val.moduleData[0]) {
                    //    this.displayedColumns.push((key));
                    //}
                    val.moduleDetails.forEach(function (val) {
                        _this.displayedColumns.push((val.ColumnName));
                    });
                    _this.displayedColumnArray.push(_this.displayedColumns);
                    //val.moduleData.forEach(element => rows.push(element, { detailRow: true, element }));
                    //    console.log(rows);
                    _this.MyDataSourceArray.push(_this.MyDataSource);
                    _this.isLoadingResults = false;
                    count++;
                }
            }); //this.dataSource = new MatTableDataSource(this.Module);
            // this.dtTrigger.next();
        });
        //this.dtOptions = {
        //    pagingType: 'full_numbers',
        //    pageLength: 10,
        //    serverSide: true,
        //    processing: true,
        //    //to restrict from ordering the data
        //    order: [],
        //    columnDefs: [{ orderable: false, targets: [0] }],
        //    ajax: (dataTablesParameters: any, callback) => {
        //        this.mainpageservice.GetPaginatedData(this.Sys_Menu_ID, this.UserName, dataTablesParameters, null).subscribe(resp => {
        //            sessionStorage.setItem('datatableparameter', JSON.stringify(dataTablesParameters));
        //            this.dt_Module = resp.data;
        //            console.log("ModuleData" + resp.data);
        //            this.show = false;
        //            callback({
        //                recordsTotal: resp.recordsTotal,
        //                recordsFiltered: resp.recordsFiltered,
        //                data: []
        //            });
        //        });
        //    },
        //    responsive: true
        //};
        //this.dtTrigger.next();
    };
    //To submit Form
    Menu7Page.prototype.onSubmit = function (form, ID) {
        var _this = this;
        var that = this;
        var Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, this.UserName).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
            //location.reload();
        });
        //Form With FileUpload
        //this.storage.get('FileType-' + this.id).then(FileType => {
        //    this.mainpageservice.Savedata1(ID, JSON.stringify(form), this.UserName, FileType).subscribe(resp => {
        //        console.log(JSON.stringify(resp["Message"]));
        //        let toast = this.toastController.create({
        //    message: resp["Message"],
        //    duration: 3000,
        //    position: 'bottom',
        //            showCloseButton: true,
        //    closeButtonText: 'Ok',
        //        });
        //        this.show = false;
        //        if (JSON.stringify(resp["Message"]) == "Record saved successfully!!")
        //            this.storage.clear();
        //        toast.then(toast => toast.present());
        //    });
        //});
    };
    Menu7Page.prototype.Savedatatable = function (form, ID) {
        var _this = this;
        var that = this;
        var Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        //Form Without FileUpload
        this.mainpageservice.SaveDatatable(ID, form, this.UserName).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
        });
        //Form With FileUpload
        //this.storage.get('FileType-' + this.id).then(FileType => {
        //    this.mainpageservice.Savedata1(ID, JSON.stringify(form), this.UserName, FileType).subscribe(resp => {
        //        console.log(JSON.stringify(resp["Message"]));
        //        let toast = this.toastController.create({
        //    message: resp["Message"],
        //    duration: 3000,
        //    position: 'bottom',
        //            showCloseButton: true,
        //    closeButtonText: 'Ok',
        //        });
        //        this.show = false;
        //        if (JSON.stringify(resp["Message"]) == "Record saved successfully!!")
        //            this.storage.clear();
        //        toast.then(toast => toast.present());
        //    });
        //});
    };
    Menu7Page.prototype.onSubmitFilter = function (formdata) {
        var _this = this;
        this.MyDataSourceArray = [];
        this.displayedColumnArray = [];
        console.log('you submitted value in Filter:', formdata);
        var data;
        data = JSON.stringify(formdata);
        data = '{Filter:' + data + '}';
        console.log(data);
        this.UserName = localStorage.getItem('username');
        sessionStorage.setItem('filterdata', data);
        this.datatableparam = sessionStorage.getItem('datatableparameter');
        console.log(this.datatableparam);
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, data).subscribe(function (filter) {
            debugger;
            console.log('filteredData', filter);
            _this.Module = filter;
            _this.MyDataSource = new MatTableDataSource();
            var rows = [];
            var count = 0;
            _this.Module[0].moduleList.forEach(function (val) {
                if (val.DisplayType === '') {
                    _this.displayedColumns = [];
                    _this.MyDataSource = new MatTableDataSource();
                    var dataArray_2 = [];
                    dataArray_2 = val.ModuleDataWithRule;
                    var resultArray = Object.keys(dataArray_2).map(function (index) {
                        var data = dataArray_2[index];
                        return data;
                    });
                    _this.MyDataSource.filterPredicate = function (data, filter) {
                        var accumulator = function (currentTerm, key) {
                            return _this.nestedFilterCheck(currentTerm, data, key);
                        };
                        var dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
                        // Transform the filter by converting it to lowercase and removing whitespace.
                        var transformedFilter = filter.trim().toLowerCase();
                        return dataStr.indexOf(transformedFilter) !== -1;
                    };
                    _this.MyDataSource.sortingDataAccessor = _this.sortingDataAccessor;
                    _this.MyDataSource.sort = _this.sort;
                    _this.MyDataSource.data = resultArray;
                    val.moduleDetails.forEach(function (val) {
                        _this.displayedColumns.push((val.ColumnName));
                    });
                    _this.displayedColumnArray.push(_this.displayedColumns);
                    _this.MyDataSourceArray.push(_this.MyDataSource);
                    _this.isLoadingResults = false;
                    count++;
                }
            });
            var paginatorArrya = _this.paginatorList.toArray();
            var sortarray = _this.sortList.toArray();
            var sortcount = 0;
            _this.MyDataSourceArray.forEach(function (item) {
                debugger;
                item.paginator = paginatorArrya[sortcount];
                item.sort = sortarray[sortcount];
                sortcount++;
            });
            //this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //    // Destroy the table first
            //    // Call the dtTrigger to rerender again
            //    dtInstance.destroy();
            //    this.dtTrigger.next();
            //});
        });
    };
    Menu7Page.prototype.downloadreport = function () {
        //alert(menuid);
        this.show = true;
        var data;
        this.UserName = localStorage.getItem('username');
        data = sessionStorage.getItem('filterdata');
        this.mainpageservice.exporttoexcel(this.Sys_Menu_ID, this.UserName, data).subscribe(function (resp) {
            console.log(resp);
            //  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            var blob = new Blob([resp], { type: 'application/pdf' });
            var d = new Date();
            var montharr = d.getMonth() + 1;
            var month = (montharr < 10 ? '0' : '') + montharr;
            var day = ((d.getDate()) < 10 ? '0' : '') + (d.getDate());
            var year = d.getFullYear();
            var x = day + '-' + month + '-' + year;
            var filename = 'Torque Report_' + x + '.xlsx';
            saveAs(blob, filename);
        });
        this.show = false;
    };
    Menu7Page.prototype.navigate = function (i, LinkedMenu) {
        var qm = { "rowval": i };
        var navigationExtras = {
            queryParams: {
                "rowval": i
            }
        };
        this.events.publish('navigationExtras', qm);
        this.router.navigate(["/menu/first/tabs/" + LinkedMenu], navigationExtras);
        // this.router.navigate(["/menu/first/tabs/GotoForm"], navigationExtras);
    };
    Menu7Page.prototype.ngAfterViewInit = function () {
        this.dtTrigger.next();
        var paginatorArrya = this.paginatorList.toArray();
        var sortarray = this.sortList.toArray();
        var count = 0;
        this.MyDataSourceArray.forEach(function (item) {
            debugger;
            item.paginator = paginatorArrya[count];
            item.sort = sortarray[count];
            count++;
        });
        // this.slides.lockSwipes(true);
    };
    Menu7Page.prototype.ngOnDestroy = function () {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    };
    Menu7Page.prototype.onWizardSubmit = function (ID) {
        var _this = this;
        debugger;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        var form = JSON.stringify(this.WizardFormValue);
        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, null).subscribe(function (resp) {
            console.log(JSON.stringify(resp["Message"]));
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
            _this.show = false;
        });
    };
    Menu7Page.prototype.Next = function (ID, wizardFormdata) {
        debugger;
        this.WizardFormValue = Object.assign(this.WizardFormValue, wizardFormdata);
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    };
    Menu7Page.prototype.Prev = function () {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    };
    Menu7Page.prototype.handleFileInput = function (files) {
        var _this = this;
        this.UserName = localStorage.getItem('username');
        this.fileToUpload = files.item(0);
        //To display loader
        this.show = true;
        this.mainpageservice.postFile(this.fileToUpload, this.UserName, 'FileUpload').subscribe(function (resp) {
            _this.show = false;
            console.log(JSON.stringify(resp["Message"]));
            //To display loader
            var toast = _this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
            });
            toast.then(function (toast) { return toast.present(); });
        });
    };
    Menu7Page.prototype._handleReaderLoaded = function (readerEvt) {
        var binaryString = readerEvt.target.result;
        this.id = Math.random();
        sessionStorage.setItem('id', this.id);
        //this.base64textString = btoa(binaryString);
        console.log(btoa(binaryString));
        this.storage.set('sampler-' + this.id, btoa(binaryString));
        var FileType = {
            name: this.FileName,
            FileData: this.b64toBlob(btoa(binaryString), '', '')
        };
        console.log(btoa(binaryString));
        this.storage.set('FileType-' + this.id, FileType);
        var toast = this.toastController.create({
            message: "Your image is stored. Click on Submit to save the form.",
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Ok',
        });
        toast.then(function (toast) { return toast.present(); });
        console.log('blob data while passing to savedata1' + JSON.stringify(FileType));
    };
    Menu7Page.prototype.b64toBlob = function (b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    Menu7Page.prototype.RenderDataTable = function () {
        var _this = this;
        this.show = true;
        this.UserName = localStorage.getItem('username');
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID, this.UserName, undefined)
            .subscribe(function (res) {
            debugger;
            _this.MyDataSource = new MatTableDataSource();
            _this.Module = res;
            // const rows = [];
            var count = 0;
            //res.forEach(element => rows.push(element, { detailRow: true, element }));
            //console.log(rows);
            _this.Module[0].moduleList.forEach(function (val) {
                //if (val.DisplayType === '') {
                _this.MyDataSource = new MatTableDataSource();
                _this.MyDataSource.data = val.moduleData;
                //val.moduleData.forEach(element => rows.push(element, { detailRow: true, element }));
                //    console.log(rows);
                _this.MyDataSourceArray.push(_this.MyDataSource);
                _this.isLoadingResults = false;
                count++;
                //}
                //else {
                //    alert(val.DisplayType);
                //}
            });
        }, function (error) {
            console.log('There was an error while retrieving Posts !!!' + error);
        });
    };
    Menu7Page.prototype.applyFilterForTable = function (filterValue, index) {
        this.MyDataSourceArray[index].filter = filterValue.trim().toLowerCase();
        if (this.MyDataSourceArray[index].paginator) {
            this.MyDataSourceArray[index].paginator.firstPage();
        }
    };
    Menu7Page.prototype.GenerateReport = function (Rowval, ModuleDetailId, ReportType) {
        var data;
        this.UserName = localStorage.getItem('username');
        data = '{Rowval:"' + Rowval + '",' + 'ModuleDetailId:' + ModuleDetailId + '}';
        this.mainpageservice.GenerateReport(ReportType, this.UserName, JSON.stringify(data)).subscribe(function (resp) {
            console.log(resp);
            //  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            var blob = new Blob([resp], { type: 'application/pdf' });
            var d = new Date();
            var montharr = d.getMonth() + 1;
            var month = (montharr < 10 ? '0' : '') + montharr;
            var day = ((d.getDate()) < 10 ? '0' : '') + (d.getDate());
            var year = d.getFullYear();
            var x = year + '-' + month + '-' + day;
            var filename = ReportType + x + '.pdf';
            saveAs(blob, filename);
        });
    };
    Menu7Page.prototype.SendReport = function (Rowval, ModuleDetailId, ReportType) {
        var data;
        this.UserName = localStorage.getItem('username');
        data = '{Rowval:"' + Rowval + '",' + 'ModuleDetailId:' + ModuleDetailId + '}';
        this.mainpageservice.SendReport(ReportType, this.UserName, JSON.stringify(data)).subscribe(function (resp) {
        });
    };
    Menu7Page.prototype.GenerateQRcode = function (Rowval, ModuleDetailId, ReportType) {
        var data;
        this.UserName = localStorage.getItem('username');
        data = '{Rowval:"' + Rowval + '",' + 'ModuleDetailId:' + ModuleDetailId + '}';
        this.mainpageservice.GenerateQRcode(ReportType, this.UserName, JSON.stringify(data)).subscribe(function (resp) {
            console.log(resp);
            //  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
            var blob = new Blob([resp], { type: 'application/pdf' });
            var d = new Date();
            var montharr = d.getMonth() + 1;
            var month = (montharr < 10 ? '0' : '') + montharr;
            var day = ((d.getDate()) < 10 ? '0' : '') + (d.getDate());
            var year = d.getFullYear();
            var x = year + '-' + month + '-' + day;
            var filename = Rowval + x + '.png';
            saveAs(blob, filename);
            var url = 'http://localhost:52371/Content/QRcodes/' + Rowval + '.png';
            var Pagelink = "about:blank";
            var pwa = window.open(Pagelink, "_new");
            pwa.document.open();
            pwa.document.write('<html><head><script>function step1(){\n setTimeout("step2()", 10);}\n function step2(){window.print();window.close()}\n </script></head><body onload="step1()">\n <img src=' + url + ' align ="center" style = "top:45%;position:relative:vertical-align:middle;width: 100%;height: 100%" /></body ></html >');
            pwa.document.close();
        });
    };
    __decorate([
        ViewChild('slides'),
        __metadata("design:type", IonSlides)
    ], Menu7Page.prototype, "slides", void 0);
    __decorate([
        ViewChild(DataTableDirective),
        __metadata("design:type", DataTableDirective)
    ], Menu7Page.prototype, "dtElement", void 0);
    __decorate([
        ViewChildren(MatPaginator),
        __metadata("design:type", QueryList)
    ], Menu7Page.prototype, "paginatorList", void 0);
    __decorate([
        ViewChildren(MatSort),
        __metadata("design:type", QueryList)
    ], Menu7Page.prototype, "sortList", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], Menu7Page.prototype, "sort", void 0);
    Menu7Page = __decorate([
        Component({
            selector: 'app-menu7',
            templateUrl: './menu7.component.html',
            styleUrls: ['./menu7.component.scss'],
        }),
        __metadata("design:paramtypes", [MainPageService,
            Router,
            ToastController,
            ActivatedRoute,
            NgZone,
            Events,
            Storage //Added
        ])
    ], Menu7Page);
    return Menu7Page;
}());
export { Menu7Page };
//# sourceMappingURL=menu7.component.js.map