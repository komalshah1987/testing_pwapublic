import { MainpagePage } from './mainpage.page';
import { Menu1Page } from './menu1/menu1.component';
import { Menu2Page } from './menu2/menu2.component';
import { Menu4Page } from './menu4/menu4.component';
import { Menu5Page } from './menu5/menu5.component';
import { Menu6Page } from './menu6/menu6.component';
import { Menu7Page } from './menu7/menu7.component';
import { Menu1007Page } from './menu1007/menu1007.component';
import { Menu1009Page } from './menu1009/menu1009.component';
import { Menu1010Page } from './menu1010/menu1010.component';
import { Menu1011Page } from './menu1011/menu1011.component';
import { Menu1012Page } from './menu1012/menu1012.component';
import { Menu1013Page } from './menu1013/menu1013.component';
import { Menu1014Page } from './menu1014/menu1014.component';
import { Menu1015Page } from './menu1015/menu1015.component';
import { Menu1016Page } from './menu1016/menu1016.component';
import { Menu1017Page } from './menu1017/menu1017.component';
export var routes = [
    {
        path: 'tabs',
        component: MainpagePage,
        children: [
            {
                path: '1',
                component: Menu1Page
            },
            {
                path: '2',
                component: Menu2Page
            },
            {
                path: '4',
                component: Menu4Page
            },
            {
                path: '5',
                component: Menu5Page
            },
            {
                path: '6',
                component: Menu6Page
            },
            {
                path: '7',
                component: Menu7Page
            },
            {
                path: '1007',
                component: Menu1007Page
            },
            {
                path: '1009',
                component: Menu1009Page
            },
            {
                path: '1010',
                component: Menu1010Page
            },
            {
                path: '1011',
                component: Menu1011Page
            },
            {
                path: '1012',
                component: Menu1012Page
            },
            {
                path: '1013',
                component: Menu1013Page
            },
            {
                path: '1014',
                component: Menu1014Page
            },
            {
                path: '1015',
                component: Menu1015Page
            },
            {
                path: '1016',
                component: Menu1016Page
            },
            {
                path: '1017',
                component: Menu1017Page
            },
        ]
    }
];
//# sourceMappingURL=mainpage.routes.js.map