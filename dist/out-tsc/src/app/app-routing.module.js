import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from './Services/auth-guard.service';
var routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    // { path: 'core', loadChildren: './core/core.module#CorePageModule' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    {
        path: 'menu',
        canActivate: [AuthGuardService],
        loadChildren: './menu/menu.module#MenuPageModule'
    },
    { path: 'header', loadChildren: './header/header.module#HeaderPageModule' },
    { path: 'footer', loadChildren: './footer/footer.module#FooterPageModule' },
    { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' }
    //{ path: 'registration', loadChildren: './mainpage/mainpage.module#MainpagePageModule' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map