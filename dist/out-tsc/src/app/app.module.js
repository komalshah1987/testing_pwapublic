import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuService } from './Services/menu.service';
import { DataTablesModule } from 'angular-datatables';
import { AuthGuardService } from './Services/auth-guard.service';
import { AuthenticationService } from './Services/authentication.service';
import { LoginService } from './Services/Login.service';
import { HeaderPageModule } from './header/header.module';
import { Data } from './Services/data.service';
import { MdePopoverModule } from '@material-extended/mde';
import { MatStepperModule, MatInputModule, MatButtonModule } from '@angular/material';
import { NgQrScannerModule } from 'angular2-qrscanner';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [AppComponent],
            entryComponents: [],
            imports: [BrowserModule, BrowserAnimationsModule,
                MatStepperModule, MatInputModule, MatButtonModule,
                HeaderPageModule, MdePopoverModule,
                IonicModule.forRoot(), NgQrScannerModule,
                AppRoutingModule, HttpClientModule, DataTablesModule],
            providers: [
                StatusBar,
                SplashScreen,
                MenuService,
                LoginService,
                AuthenticationService,
                AuthGuardService,
                Data,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map