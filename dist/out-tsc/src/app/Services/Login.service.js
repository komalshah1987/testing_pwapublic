import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';
var apiUrl = environment.apiUrl;
var LoginService = /** @class */ (function () {
    function LoginService(_http) {
        this._http = _http;
    }
    LoginService.prototype.getUserDetails = function (UserName, Password) {
        return this._http.get(apiUrl + "/" +
            ("api/Login/ValidateUser?UserName=" + UserName + "&Password=" + Password))
            .pipe(catchError(this.errorhandler));
    };
    LoginService.prototype.errorhandler = function (error) {
        return throwError(error.message);
    };
    LoginService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], LoginService);
    return LoginService;
}());
export { LoginService };
//# sourceMappingURL=Login.service.js.map