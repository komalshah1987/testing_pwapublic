import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(authentication, router) {
        this.authentication = authentication;
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function () {
        var token = this.authentication.getToken();
        //let accessToken = this.authentication.getAccessToken();
        if (!token) {
            console.error("User is not authenticated.");
            this.redirectToLoginPage();
            return false;
        }
        else if (this.authentication.isAuthenticated()) {
            return true;
        }
        else {
            this.authentication.refreshToken();
            return true;
        }
    };
    AuthGuardService.prototype.redirectToLoginPage = function () {
        this.router.navigate(['login']);
    };
    AuthGuardService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AuthenticationService, Router])
    ], AuthGuardService);
    return AuthGuardService;
}());
export { AuthGuardService };
//# sourceMappingURL=auth-guard.service.js.map