import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Headers, RequestOptions, ResponseContentType } from "@angular/http";
import { throwError } from 'rxjs';
import { catchError } from "rxjs/operators";
import { environment } from '../../environments/environment';
//import { ServerSideColumn } from '../Models/ServersideColumn';
//import { Module } from '../Models/Module';
//import { Observable } from 'rxjs/Observable';
var apiUrl = environment.apiUrl;
var DataTablesResponse = /** @class */ (function () {
    function DataTablesResponse() {
    }
    return DataTablesResponse;
}());
var MainPageService = /** @class */ (function () {
    function MainPageService(_http) {
        this._http = _http;
        this.headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.dtOptions = {};
        this.onlineFlag = navigator.onLine;
        console.log(this.onlineFlag);
    }
    //getListModule(MenuID, UserName): Observable<Module[]> {
    //    return this._http.get<Module[]>(`${apiUrl}/${'api/Page/GetListModule?UserName=' + UserName + '&MenuNo=' + MenuID}`);
    //}
    MainPageService.prototype.getmodules = function (MenuID, UserName) {
        //alert('getmodules');
        //console.log(apiUrl);
        return this._http.get(apiUrl + "/" + ('api/Page/GetModules?Id=' + MenuID + '&UserName=' + UserName));
    };
    MainPageService.prototype.getdata = function () {
        return this._http.get("" + 'https://jsonplaceholder.typicode.com/photos');
    };
    //GetColumnsForDataTable(MenuID, UserName): Observable<ServerSideColumn[]> {
    //    return this._http.get<ServerSideColumn[]>(`${apiUrl}/${'api/Page/GetColumnsForDataTable_AG_6?Id=' + MenuID + '&UserName=' + UserName}`, { responseType: 'json' });
    //}
    MainPageService.prototype.getPageMenuDetails = function (MenuID, UserName) {
        //alert('call');
        return this._http.get(apiUrl + "/" + ('api/Page/GetPageMenuDetail?UserName=' + UserName + '&MenuNo=' + MenuID));
    };
    MainPageService.prototype.GetPaginatedData = function (MenuID, UserName, dataTablesParameters, data) {
        //alert(dataTablesParameters);
        data = sessionStorage.getItem('filterdata');
        //alert("You are in main pageservice paginated" +data);
        console.log('dataTablesParameters', dataTablesParameters);
        console.log('GetPaginatedData');
        var body = new HttpParams()
            .set('datatable', JSON.stringify(dataTablesParameters))
            .set('filterData', data);
        ResponseContentType.ArrayBuffer;
        return this._http.post(apiUrl + "/" + ('api/Page/GetPaginatedData1?Id=' + MenuID +
            '&UserName=' + UserName), body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
        //return this._http.post<DataTablesResponse>(`${apiUrl}/${'api/Page/GetPaginatedData1?Id=' + MenuID + 
        //'&UserName=' + UserName}`, dataTablesParameters, data);
    };
    MainPageService.prototype.GetfilterPaginatedData = function (MenuID, UserName, dataTablesParameters, data) {
        //alert("You are in main pageservice paginated1" +data);
        //alert(dataTablesParameters);
        sessionStorage.setItem('filterdata', data);
        console.log('dataTablesParameters', dataTablesParameters);
        console.log('GetPaginatedData');
        var body = new HttpParams()
            .set('datatable', dataTablesParameters)
            .set('filterData', data);
        return this._http.post(apiUrl + "/" + ('api/Page/GetPaginatedData1?Id=' + MenuID +
            '&UserName=' + UserName), body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
        //return this._http.post<DataTablesResponse>(`${apiUrl}/${'api/Page/GetPaginatedData1?Id=' + MenuID + 
        //'&UserName=' + UserName}`, dataTablesParameters, data);
    };
    MainPageService.prototype.buildDtOptions = function () {
        return {
            pagingType: 'full_numbers',
            pageLength: 10,
            processing: true,
            "language": {
                "processing": "Hang on. Waiting for response..." //add a loading image,simply putting <img src="loader.gif" /> tag.
            },
            responsive: true,
            //to restrict from ordering the data
            order: [],
            columnDefs: [{ orderable: false, targets: [0] }]
        };
    };
    MainPageService.prototype.Savedata = function (ModuleID, data, username) {
        return this._http.post(apiUrl + "/" + ('api/Page/SaveModules?id=' + ModuleID + '&username=' + username), data);
    };
    MainPageService.prototype.SaveDatatable = function (ModuleID, data, username) {
        return this._http.post(apiUrl + "/" + ('api/Page/SaveDataTable?id=' + ModuleID + '&username=' + username), data);
    };
    MainPageService.prototype.Savedata1 = function (ModuleID, data, username, httpdata) {
        //debugger;
        //alert("You are in Mainpage.service.ts " +username);
        var formData = new FormData();
        console.log('data :-' + data);
        console.log('File Name :- ' + httpdata.name + ' File Data :-' + httpdata.FileData);
        formData.append('data', data);
        formData.append(httpdata.name, httpdata.FileData);
        console.log(JSON.stringify(formData));
        return this._http.post(apiUrl + "/" + ('api/Page/SaveModules?id=' + ModuleID + '&username=' + username), formData);
    };
    MainPageService.prototype.Savedata1_backup = function (ModuleID, data, username, httpdata) {
        // alert("You are in Mainpage.service.ts " +username);
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'multipart/form-data');
        var requestOptions = new RequestOptions({ headers: headers });
        data.append('blob', httpdata);
        console.log(data);
        //data.append('blob',httpdata);
        return this._http.post(apiUrl + "/" + ('api/Page/SaveModules?id=' + ModuleID + '&username=' + username), data);
    };
    // return this._http.post(`${apiUrl}/${'api/Page/SaveModules?id='+ModuleID+'&username=' +username}`, data)
    MainPageService.prototype.postFile = function (fileToUpload, username, columnHeader) {
        var data = new FormData();
        data.append('fileKey', fileToUpload);
        // const data='{"file":"'+fileToUpload.name+'""}'
        console.log(data);
        return this._http.post(apiUrl + "/" + ('api/Page/UploadJsonFile?columnHeader=' + columnHeader + '&UserName=' + username), data); /*
        .subscribe(
        data  => {
        console.log("POST Request is successful ", data);
        },
        error  => {

        console.log("Error", error);

        }

        );*/
    };
    MainPageService.prototype.postFile1 = function (data, username, columnHeader) {
        return this._http.post(apiUrl + "/" + ('api/Page/UploadJsonFile?columnHeader=' + columnHeader + '&UserName=' + username), data).pipe(catchError(this.errorhandler));
    };
    MainPageService.prototype.errorhandler = function (error) {
        console.log('error', error);
        return throwError(error.message);
    };
    MainPageService.prototype.SaveJsData = function (ModuleID, data, username) {
        var body = new HttpParams()
            .set('data', data);
        return this._http.post(apiUrl + "/" + ('api/Page/SaveModuleJson?id=' + ModuleID + '&username=' + username), body, { responseType: 'text' });
    };
    MainPageService.prototype.exporttoexcel = function (menuid, UserName, data) {
        //console.log("data",JSON.stringify(data));
        //var datasend=JSON.stringify(data);
        //const body = new HttpParams()
        //.set('data', data);
        return this._http.post(apiUrl + "/" + ('api/Page/ExportToexcel?MenuId=' + menuid + '&UserName=' + UserName), data, { responseType: 'arraybuffer' });
    };
    MainPageService.prototype.buildDtOptions1 = function () {
        var _this = this;
        return {
            pagingType: 'full_numbers',
            pageLength: 10,
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            ajax: function (dataTablesParameters, callback) {
                //let pageOffset = dataTablesParameters.start;
                //let pageSize = dataTablesParameters.length;
                //let searchText = dataTablesParameters.search != null ? dataTablesParameters.search.value : '';
                //alert('enter' + this.menuID);
                _this.GetPaginatedData(1, 'Admin', dataTablesParameters, '')
                    //this._http.post<DataTablesResponse>('http://localhost:52371/api/Page/GetPaginatedData?Id=' + this.menuID + '&UserName=' + this.UserName, dataTablesParameters, {})
                    .subscribe(function (resp) {
                    _this.Module = resp.data;
                    console.log('serverside', _this.Module);
                    callback({
                        recordsTotal: resp.recordsTotal,
                        recordsFiltered: resp.recordsFiltered,
                        data: [],
                    });
                });
            },
            responsive: true
            //scroller: true
        };
    };
    MainPageService.prototype.GetModulesWithFilter = function (MenuID, UserName, data) {
        console.log("You are in getmoduleswithfilter");
        return this._http.post(apiUrl + "/" + ('api/Page/GetModulesWithFilters_v1?Id=' + MenuID + '&UserName=' + UserName), data, this.headers);
    };
    //Get Mails
    MainPageService.prototype.GetMails = function (UserName) {
        debugger;
        return this._http.get(apiUrl + "/" + ("api/Email/GetMails?username=" + UserName));
    };
    //Table Save Added On:May 17 2019
    MainPageService.prototype.SaveModule = function (ModuleID, data, username) {
        //const body=new HttpParams()
        //.set('data',data);
        //return this._http.post(`${apiUrl}/${'api/Page/SaveModuleJson?id=' + ModuleID + '&username=' + username}`, data, {responseType:'text', headers: { 'Content-Type': 'application/json; charset=utf-8'}  })
        return this._http.post(apiUrl + "/" + ('api/Page/SaveModule?id=' + ModuleID + '&username=' + username), data);
    };
    MainPageService.prototype.GenerateReport = function (reporttype, username, data) {
        //const body=new HttpParams()
        //.set('data',data);
        //return this._http.post(`${apiUrl}/${'api/Page/SaveModuleJson?id=' + ModuleID + '&username=' + username}`, data, {responseType:'text', headers: { 'Content-Type': 'application/json; charset=utf-8'}  })
        //return this._http.post(`${apiUrl}/${'api/Report/ExportReport?reporttype=' + reporttype + '&username=' + username}`, data,this.headers)
        return this._http.post(apiUrl + "/" + ('api/Report/ExportReport?reporttype=' + reporttype + '&username=' + username), data, { responseType: 'blob', headers: { 'Content-Type': 'application/json' } });
    };
    MainPageService.prototype.SendReport = function (reporttype, username, data) {
        return this._http.post(apiUrl + "/" + ('api/Report/SendReport?reporttype=' + reporttype + '&username=' + username), data, { responseType: 'blob', headers: { 'Content-Type': 'application/json' } });
    };
    MainPageService.prototype.GenerateQRcode = function (reporttype, username, data) {
        //const body=new HttpParams()
        //.set('data',data);
        //return this._http.post(`${apiUrl}/${'api/Page/SaveModuleJson?id=' + ModuleID + '&username=' + username}`, data, {responseType:'text', headers: { 'Content-Type': 'application/json; charset=utf-8'}  })
        //return this._http.post(`${apiUrl}/${'api/Report/ExportReport?reporttype=' + reporttype + '&username=' + username}`, data,this.headers)
        return this._http.post(apiUrl + "/" + ('api/Page/GetImageQRcode?reporttype=' + reporttype + '&username=' + username), data, { responseType: 'blob', headers: { 'Content-Type': 'application/json' } });
    };
    MainPageService.prototype.GetQRBatchPrint = function (MenuID, UserName) {
        return this._http.post(apiUrl + "/" + ('api/Page/GetQRBatchPrint?Id=' + MenuID + '&UserName=' + UserName), this.headers);
    };
    MainPageService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], MainPageService);
    return MainPageService;
}());
export { MainPageService };
//# sourceMappingURL=MainPage.service.js.map