import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(router) {
        this.router = router;
        //token = {
        //    refresh_token: 'refreshtokencode',
        //    exp: new Date((new Date().getDate() + 1)),
        //    access_token: {
        //        username: 'user'
        //    }
        //};
        this.tokenKey = "a6smm_utoken";
    }
    AuthenticationService.prototype.login = function (username, password) {
        var token = {
            refresh_token: 'refreshtokencode',
            user: username,
            exp: new Date((new Date().getDate() + 1)),
            access_token: {
                username: username
            }
        };
        this.setToken(token);
        localStorage.setItem('username', username);
        this.router.navigate(['menu', 'first']);
    };
    AuthenticationService.prototype.logout = function () {
        this.removeToken();
        this.router.navigate(['login']);
    };
    AuthenticationService.prototype.getToken = function () {
        return JSON.parse(localStorage.getItem(this.tokenKey));
    };
    AuthenticationService.prototype.getuser = function () {
        console.log('getuser', JSON.parse(localStorage.getItem(this.tokenKey)['user']));
    };
    AuthenticationService.prototype.setToken = function (token) {
        localStorage.setItem(this.tokenKey, JSON.stringify(token));
    };
    AuthenticationService.prototype.getAccessToken = function () {
        return JSON.parse(localStorage.getItem(this.tokenKey))['access_token'];
    };
    AuthenticationService.prototype.isAuthenticated = function () {
        var token = localStorage.getItem(this.tokenKey);
        if (token) {
            return true;
        }
        else {
            return false;
        }
    };
    AuthenticationService.prototype.refreshToken = function () {
        //this.token.exp = new Date((new Date().getDate() + 1));
        //this.setToken(this.token);
    };
    AuthenticationService.prototype.removeToken = function () {
        localStorage.removeItem(this.tokenKey);
        localStorage.removeItem('username');
    };
    AuthenticationService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Router])
    ], AuthenticationService);
    return AuthenticationService;
}());
export { AuthenticationService };
//# sourceMappingURL=authentication.service.js.map