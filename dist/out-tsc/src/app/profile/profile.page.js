import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { Events } from '@ionic/angular';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(events) {
        this.events = events;
        this.events.publish('PageName', 'Profile');
    }
    ProfilePage.prototype.ngOnInit = function () {
    };
    ProfilePage = __decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.page.html',
            styleUrls: ['./profile.page.scss'],
        }),
        __metadata("design:paramtypes", [Events])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.page.js.map