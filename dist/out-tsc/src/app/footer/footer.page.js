import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
var FooterPage = /** @class */ (function () {
    function FooterPage() {
    }
    FooterPage.prototype.ngOnInit = function () {
    };
    FooterPage = __decorate([
        Component({
            selector: 'app-footer',
            templateUrl: './footer.page.html',
            styleUrls: ['./footer.page.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], FooterPage);
    return FooterPage;
}());
export { FooterPage };
//# sourceMappingURL=footer.page.js.map