import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FooterPage } from './footer.page';
var routes = [
    {
        path: '',
        component: FooterPage
    }
];
var FooterPageModule = /** @class */ (function () {
    function FooterPageModule() {
    }
    FooterPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [FooterPage]
        })
    ], FooterPageModule);
    return FooterPageModule;
}());
export { FooterPageModule };
//# sourceMappingURL=footer.module.js.map