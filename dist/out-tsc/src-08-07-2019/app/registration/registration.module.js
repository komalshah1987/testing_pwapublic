import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
//import { IonSimpleWizard } from '../ion-simple-conditional-wizard/ion-simple-wizard.component';
//import { IonSimpleWizardStep } from '../ion-simple-conditional-wizard/ion-simple-wizard.step.component';
import { IonicModule } from '@ionic/angular';
import { RegistrationPage } from './registration.page';
import { MatStepperModule, MatInputModule, MatButtonModule } from '@angular/material';
import { CdkStepperModule } from '@angular/cdk/stepper';
var routes = [
    {
        path: '',
        component: RegistrationPage
    }
];
var RegistrationPageModule = /** @class */ (function () {
    function RegistrationPageModule() {
    }
    RegistrationPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                CdkStepperModule,
                ReactiveFormsModule,
                MatStepperModule, MatInputModule, MatButtonModule,
                RouterModule.forChild(routes)
            ],
            declarations: [RegistrationPage
                //, IonSimpleWizard,
                //IonSimpleWizardStep
            ]
        })
    ], RegistrationPageModule);
    return RegistrationPageModule;
}());
export { RegistrationPageModule };
//# sourceMappingURL=registration.module.js.map