import { __decorate, __metadata } from "tslib";
import { Component, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { IonSlides } from '@ionic/angular';
var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(navCtrl, _formBuilder) {
        this.navCtrl = navCtrl;
        this._formBuilder = _formBuilder;
        this.title = 'materialApp';
    }
    RegistrationPage.prototype.selectChange = function (e) {
        console.log(e);
    };
    RegistrationPage.prototype.Next = function () {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    };
    RegistrationPage.prototype.Prev = function () {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    };
    RegistrationPage.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', Validators.required]
        });
    };
    RegistrationPage.prototype.ngAfterViewInit = function () {
        this.slides.lockSwipes(true);
    };
    __decorate([
        ViewChild('slides'),
        __metadata("design:type", IonSlides)
    ], RegistrationPage.prototype, "slides", void 0);
    RegistrationPage = __decorate([
        Component({
            selector: 'app-registration',
            templateUrl: './registration.page.html',
            styleUrls: ['./registration.page.scss'],
        }),
        __metadata("design:paramtypes", [NavController, FormBuilder])
    ], RegistrationPage);
    return RegistrationPage;
}());
export { RegistrationPage };
//# sourceMappingURL=registration.page.js.map