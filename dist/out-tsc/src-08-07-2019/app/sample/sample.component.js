import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
var SampleComponent = /** @class */ (function () {
    function SampleComponent() {
    }
    SampleComponent.prototype.ngOnInit = function () { };
    SampleComponent = __decorate([
        Component({
            selector: 'app-sample',
            templateUrl: './sample.component.html',
            styleUrls: ['./sample.component.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], SampleComponent);
    return SampleComponent;
}());
export { SampleComponent };
//# sourceMappingURL=sample.component.js.map