import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';
import { LoginService } from '../Services/Login.service';
import { Events } from '@ionic/angular';
import { Data } from "../Services/data.service";
var LoginPage = /** @class */ (function () {
    function LoginPage(authentication, loginservice, events, data) {
        this.authentication = authentication;
        this.loginservice = loginservice;
        this.events = events;
        this.data = data;
        this.events.publish('PageName', 'login');
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.loginUser = function (e) {
        var _this = this;
        e.preventDefault();
        console.log(e);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
        this.loginservice.getUserDetails(username, password)
            .subscribe(function (data) {
            _this.User = data;
            console.log(_this.User);
            if (_this.User.Error != null) {
                _this.error = _this.User.Error;
            }
            else {
                _this.data.storage = {
                    "firstname": "Nic",
                    "lastname": "Raboy",
                    "address": {
                        "city": "San Francisco",
                        "state": "California"
                    }
                };
                _this.authentication.login(username, password);
            }
        }, function (error) {
            _this.error = error;
        });
    };
    LoginPage = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        __metadata("design:paramtypes", [AuthenticationService, LoginService,
            Events,
            Data])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map