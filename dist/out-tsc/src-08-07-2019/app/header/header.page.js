import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { Events } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Services/authentication.service';
import { environment } from '../../environments/environment';
var _AppName = environment.AppName;
var HeaderPage = /** @class */ (function () {
    function HeaderPage(events, router, authentication) {
        var _this = this;
        this.events = events;
        this.router = router;
        this.authentication = authentication;
        this.AppName = _AppName;
        events.subscribe('PageName', function (name) {
            // user and time are the same arguments passed in `events.publish(user, time)`
            console.log('name', name);
            _this.name = name;
        });
    }
    HeaderPage.prototype.ngOnInit = function () {
    };
    HeaderPage.prototype.GoToProfile = function () {
        alert('profile');
        this.router.navigate(['profile']);
    };
    HeaderPage.prototype.logout = function () {
        this.authentication.logout();
    };
    HeaderPage = __decorate([
        Component({
            selector: 'app-header',
            templateUrl: './header.page.html',
            styleUrls: ['./header.page.scss'],
        }),
        __metadata("design:paramtypes", [Events, Router,
            AuthenticationService])
    ], HeaderPage);
    return HeaderPage;
}());
export { HeaderPage };
//# sourceMappingURL=header.page.js.map