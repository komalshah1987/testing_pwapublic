import { __decorate, __metadata } from "tslib";
import { Component } from '@angular/core';
import { MenuService } from '../Services/menu.service';
import { Router } from '@angular/router';
import { Data } from "../Services/data.service";
import { Events } from '@ionic/angular';
var MenuPage = /** @class */ (function () {
    function MenuPage(menuservice, router, data, events) {
        this.menuservice = menuservice;
        this.router = router;
        this.data = data;
        this.events = events;
        this.events.publish('PageName', '');
        // alert(JSON.stringify(this.data.storage));
    }
    MenuPage.prototype.ngOnInit = function () {
        var _this = this;
        this.UserName = localStorage.getItem('username');
        console.log(this.UserName);
        //this.UserName = 'Admin';
        this.menuservice.getSubGroup(this.UserName).subscribe(function (data) {
            console.log("api", data);
            _this.MenuData = data;
            console.log("Menudata1", _this.MenuData);
        });
    };
    MenuPage = __decorate([
        Component({
            selector: 'app-menu',
            templateUrl: './menu.page.html',
            styleUrls: ['./menu.page.scss'],
        }),
        __metadata("design:paramtypes", [MenuService, Router,
            Data, Events])
    ], MenuPage);
    return MenuPage;
}());
export { MenuPage };
//# sourceMappingURL=menu.page.js.map