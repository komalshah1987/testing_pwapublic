import { MainpagePage } from './mainpage.page';
import { Menu7Page } from './menu7/menu7.component';
import { Menu2Page } from './menu2/menu2.component';
import { Menu8Page } from './menu8/menu8.component';
import { Menu3Page } from './menu3/menu3.component';
import { Menu10Page } from './menu10/menu10.component';
import { Menu11Page } from './menu11/menu11.component';
import { Menu14Page } from './menu14/menu14.component';
import { Menu15Page } from './menu15/menu15.component';
import { Menu18Page } from './menu18/menu18.component';
import { Menu21Page } from './menu21/menu21.component';
export var routes = [
    {
        path: 'tabs',
        component: MainpagePage,
        children: [
            {
                path: '7',
                component: Menu7Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '2',
                component: Menu2Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '8',
                component: Menu8Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '3',
                component: Menu3Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '10',
                component: Menu10Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '11',
                component: Menu11Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '14',
                component: Menu14Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '15',
                component: Menu15Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '18',
                component: Menu18Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            },
            {
                path: '21',
                component: Menu21Page
                //loadChildren: './menu17/menu17.module#Menu17PageModule'
            }
        ]
    }
];
//# sourceMappingURL=mainpage.routes.js.map