import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
//import { Observable } from 'rxjs/Observable';
var apiUrl = environment.apiUrl;
var MenuService = /** @class */ (function () {
    function MenuService(_http) {
        this._http = _http;
    }
    MenuService.prototype.getSubGroup = function (UserName) {
        console.log(apiUrl);
        return this._http.get(apiUrl + "/" + ("api/Menu/GetSubGroup_ng7?username=" + UserName));
    };
    MenuService.prototype.getmenus = function (id, UserName) {
        console.log(apiUrl);
        //console.log(url);
        return this._http.get(apiUrl + "/" + ("api/Menu/GetMenus?SubGroupID=" + id + "&username=" + UserName));
    };
    MenuService.prototype.getJSON = function () {
        return this._http.get("./assets/mainpage_routing.json", { responseType: 'text' });
    };
    MenuService.prototype.getAllMenus = function (UserName) {
        return this._http.get(apiUrl + "/" + ("api/Menu/GetAllMenus?username=" + UserName));
    };
    MenuService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], MenuService);
    return MenuService;
}());
export { MenuService };
//# sourceMappingURL=menu.service.js.map