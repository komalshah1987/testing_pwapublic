import { __decorate, __metadata } from "tslib";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { from, of, forkJoin } from 'rxjs';
import { switchMap, finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { MainPageService } from '../services/MainPage.service';
//import { environment } from '../../environments/environment';
var STORAGE_REQ_KEY = 'storedreq';
var OfflineManagerService = /** @class */ (function () {
    function OfflineManagerService(storage, http, mainpageservice, toastController) {
        this.storage = storage;
        this.http = http;
        this.mainpageservice = mainpageservice;
        this.toastController = toastController;
    }
    OfflineManagerService.prototype.checkForEvents = function () {
        var _this = this;
        debugger;
        return from(this.storage.get(STORAGE_REQ_KEY)).pipe(switchMap(function (storedOperations) {
            var storedObj = JSON.parse(storedOperations);
            if (storedObj && storedObj.length > 0) {
                console.log('Local data succesfully synced to API!');
                console.log(storedObj);
                return _this.sendRequests(storedObj).pipe(finalize(function () {
                    var toast = _this.toastController.create({
                        message: "Local data succesfully synced to API!",
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.then(function (toast) { return toast.present(); });
                    _this.storage.remove(STORAGE_REQ_KEY);
                }));
            }
            else {
                console.log('no local events to sync');
                return of(false);
            }
        }));
    };
    OfflineManagerService.prototype.checkForEventsOnClick = function () {
        var _this = this;
        debugger;
        return from(this.storage.get(STORAGE_REQ_KEY)).pipe(switchMap(function (storedOperations) {
            var storedObj = JSON.parse(storedOperations);
            if (storedObj && storedObj.length > 0) {
                console.log('Local data succesfully synced to API!');
                console.log(storedObj);
                return _this.sendRequests(storedObj).pipe(finalize(function () {
                    var toast = _this.toastController.create({
                        message: "Local data succesfully synced to API!",
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.then(function (toast) { return toast.present(); });
                    _this.storage.remove(STORAGE_REQ_KEY);
                }));
            }
            else {
                console.log('no local events to sync');
                var toast = _this.toastController.create({
                    message: "No local events to sync",
                    duration: 3000,
                    position: 'bottom'
                });
                toast.then(function (toast) { return toast.present(); });
                return of(false);
            }
        }));
    };
    OfflineManagerService.prototype.storeRequest = function (url, type, data, id) {
        var _this = this;
        var toast = this.toastController.create({
            message: "Your data is stored locally because you seem to be offline.",
            duration: 3000,
            position: 'bottom'
        });
        toast.then(function (toast) { return toast.present(); });
        var action = {
            url: url,
            type: type,
            data: data,
            body: data,
            id: id,
            headers: '{"Accept":"application/json, text/plain, */*","Content-Type":"multipart/form-data"}'
            //time: new Date().getTime(),
            //id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
        };
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        return this.storage.get(STORAGE_REQ_KEY).then(function (storedOperations) {
            var storedObj = JSON.parse(storedOperations);
            if (storedObj) {
                console.log('storage :' + storedObj);
                storedObj.push(action);
            }
            else {
                storedObj = [action];
            }
            // Save old & new local transactions back to Storage
            return _this.storage.set(STORAGE_REQ_KEY, JSON.stringify(storedObj));
        });
    };
    OfflineManagerService.prototype.sendRequests = function (operations) {
        var _this = this;
        var obs = [];
        debugger;
        var data;
        var httpdata;
        var form1 = new FormData();
        this.storage.get('FileType').then(function (FileType) {
            httpdata = FileType;
            form1.append(FileType.name, FileType.data);
        });
        this.storage.get('data').then(function (storeddata) {
            data = JSON.stringify(storeddata);
            form1.append('data', JSON.stringify(storeddata));
            if (httpdata != undefined && data != undefined) {
                /*  let oneObs =
                 this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com',httpdata ).subscribe(response => {});
                  //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                 //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                 obs.push(oneObs);  */
            }
        });
        var _loop_1 = function (op) {
            console.log(op.id);
            this_1.storage.get('FileType-' + op.id).then(function (FileType) {
                httpdata = FileType;
                form1.append(FileType.name, FileType.data);
                _this.storage.get('data-' + op.id).then(function (storeddata) {
                    data = JSON.stringify(storeddata);
                    form1.append('data', JSON.stringify(storeddata));
                    if (httpdata != undefined && data != undefined) {
                        console.log(httpdata);
                        console.log(data);
                        var oneObs = _this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com', httpdata).subscribe(function (response) { });
                        //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                        //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                        obs.push(oneObs);
                    }
                });
            });
        };
        var this_1 = this;
        for (var _i = 0, operations_1 = operations; _i < operations_1.length; _i++) {
            var op = operations_1[_i];
            _loop_1(op);
        }
        // Send out all local events and return once they are finished
        return forkJoin(obs);
    };
    OfflineManagerService.prototype.storeRequest1 = function (url, type, data, id) {
        var _this = this;
        var toast = this.toastController.create({
            message: "Your data is stored locally because you seem to be offline.",
            duration: 3000,
            position: 'bottom'
        });
        toast.then(function (toast) { return toast.present(); });
        var action = {
            url: url,
            type: type,
            data: data,
            body: data,
            id: id,
            headers: '{"Accept":"application/json, text/plain, */*","Content-Type":"multipart/form-data"}'
            //time: new Date().getTime(),
            //id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
        };
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        return this.storage.get(STORAGE_REQ_KEY).then(function (storedOperations) {
            var storedObj = JSON.parse(storedOperations);
            if (storedObj) {
                console.log('storage :' + storedObj);
                storedObj.push(action);
            }
            else {
                storedObj = [action];
            }
            // Save old & new local transactions back to Storage
            return _this.storage.set(STORAGE_REQ_KEY, JSON.stringify(storedObj));
        });
    };
    OfflineManagerService.prototype.sendRequests1 = function (operations) {
        var _this = this;
        var obs = [];
        debugger;
        var data;
        var httpdata;
        var form1 = new FormData();
        this.storage.get('FileType').then(function (FileType) {
            httpdata = FileType;
            form1.append(FileType.name, FileType.data);
        });
        this.storage.get('data').then(function (storeddata) {
            data = JSON.stringify(storeddata);
            form1.append('data', JSON.stringify(storeddata));
            if (httpdata != undefined && data != undefined) {
                var oneObs = _this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com', httpdata).subscribe(function (response) { });
                //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                obs.push(oneObs);
            }
        });
        for (var _i = 0, operations_2 = operations; _i < operations_2.length; _i++) {
            var op = operations_2[_i];
            /* console.log('Make one request: ', op);
        
           var newdata = this.getLocal('data');
           var newhttpdata = this.getLocal('FileType');
            console.log(form1);
            if(httpdata != undefined && data != undefined){
            let oneObs =
            this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com',httpdata )
             //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
            //= this.http.request(op.type, op.url, data:form1, { body:form1 });
            obs.push(oneObs);
            } */
            /* this.mainpageservice.Savedata1(37,"TestFromOfflineManager", 'samadhan@gmail.com', op.data).subscribe(resp=>{
      
              console.log(JSON.stringify(resp["Message"]));
              let toast = this.toastController.create({
                message:resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
        
              });
              toast.then(toast => toast.present());
            }); */
        }
        // Send out all local events and return once they are finished
        return forkJoin(obs);
    };
    OfflineManagerService.prototype.getLocal = function (key) {
        console.log('getlocaldata' + this.storage.get("" + key));
        var getdata = this.storage.get("" + key).then;
        return this.storage.get("" + key);
    };
    OfflineManagerService.prototype.getStoredData = function () {
        return this.storage.get('data').then(function (val) {
            console.dir(val);
            return val;
        });
    };
    OfflineManagerService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Storage, HttpClient, MainPageService, ToastController])
    ], OfflineManagerService);
    return OfflineManagerService;
}());
export { OfflineManagerService };
//# sourceMappingURL=offline-manager.service.js.map