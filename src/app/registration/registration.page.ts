import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController} from '@ionic/angular'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonSlides } from '@ionic/angular';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
    @ViewChild('slides') slides: IonSlides;
    constructor(public navCtrl: NavController, private _formBuilder: FormBuilder) { }
    selectChange(e) {
        console.log(e);
    }
    title = 'materialApp';
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;

    Next() {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    }
    Prev() {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    }
    ngOnInit() {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', Validators.required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', Validators.required]
        });
    }
    ngAfterViewInit() {
        this.slides.lockSwipes(true);
    }
    
}
