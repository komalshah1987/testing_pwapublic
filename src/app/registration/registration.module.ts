import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import { IonSimpleWizard } from '../ion-simple-conditional-wizard/ion-simple-wizard.component';
//import { IonSimpleWizardStep } from '../ion-simple-conditional-wizard/ion-simple-wizard.step.component';
import { IonicModule } from '@ionic/angular';

import { RegistrationPage } from './registration.page';
import {
    MatStepperModule, MatInputModule, MatButtonModule
} from '@angular/material';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { ArchwizardModule } from 'angular-archwizard';
const routes: Routes = [
  {
    path: '',
    component: RegistrationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      IonicModule,
      CdkStepperModule,
      ReactiveFormsModule,
      MatStepperModule, MatInputModule, MatButtonModule,
    RouterModule.forChild(routes)
  ],
    declarations: [RegistrationPage
        //, IonSimpleWizard,
        //IonSimpleWizardStep
    ]
})
export class RegistrationPageModule {}
