import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, Input, ViewChildren, QueryList, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Subject, Observable, fromEvent, merge, of } from 'rxjs';

import { DataTableDirective } from 'angular-datatables';
import { NavigationExtras } from '@angular/router';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { mapTo, filter, map } from 'rxjs/operators';

import { IonicSelectableComponent } from 'ionic-selectable';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { NavController  } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { NgZone } from "@angular/core"; //added
import { Storage } from '@ionic/storage';//added
import { IonSlides } from '@ionic/angular';
import { MatPaginator, MatSort, MatTableDataSource, MatTable } from '@angular/material';
import { trigger, state, transition, style, animate } from '@angular/animations';

//import { MapsAPILoader, MouseEvent } from '@agm/core';
//import { GeocoderModule } from 'angular-geocoder';
import * as $ from 'jquery';
import { parse } from 'path';
import {FileSystemNode} from '../assets/data/file-system-node';
import { TTEditableColumn } from 'primeng/treetable/treetable';
import { TreeNode } from 'primeng/api/treenode';
import { MainPageService } from './Services/MainPage.service';

interface FileObject
{  name: string,
  FileData: Blob
}


export class BaseClass implements OnInit {
    Sys_Menu_ID_Base: number;
    UserName: string;
    @ViewChild('slides') slides: IonSlides;
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject<any>();
    dt_Module: any;
    Module: any;
    zoom: number;
    address: string;
  
    @ViewChild('search')
    showfilter: any;
    public searchElementRef: ElementRef;
    //public minDate: any = new Date().toISOString();
    public minDate: any = "";
    public maxDate: any = "2299-12-31";
    FileName: any;//Added

    datatableparam: any = {};
    DropDown: any = {};
    PageMenuDetails: any;
    id: any;//Added
    WizardFormValue: any = {};
    @ViewChildren(MatPaginator) paginatorList: QueryList<MatPaginator>;
    @ViewChild(MatSort) sort: MatSort;
    // @ViewChildren(MatPaginator) paginatorList = new QueryList<MatPaginator>();
    @ViewChildren(MatSort) sortList: QueryList<MatSort>;
    MyDataSourceArray: Array<any> = [];
    isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
    expandedElementArray: Array<any> = [];
    expandedElement: any;
    isLoadingResults = true;
    MyDataSource: any;
    displayedColumnArray: Array<any> = [];
    displayedColumns: any[] = [];
    Feedback: any;
    ModuleIDList: Array<any> = [];
    ModuleDetailIDList: Array<any> = [];
    childColumnHeadersArray:Array<any> = [];
    childColumnHeaders:any[] = [];
    childColumnArray: Array<any> = [];
    childColumns: any[] = [];
    nextLevelData:any[]=[];
    public show: boolean = false;
    fileToUpload: File = null;

    //-accordions-----
    step = 0;
    showContainerEmail: boolean = false;
    //--
    //EmailwithOTP
    ValidatedEmail: boolean = false;
    VerifiedEmailId: any;
    files: FileSystemNode[] = [];
    cols: any[];
    colsdisplay :any[];
    editableCols: any[];
    totalRecords:number;
    loading:boolean;
    base64textString: string;

    @ViewChildren('LatLong') latlong: QueryList<ElementRef>;
    lat: number;
    long: number;
    lat_long: string;
    
    constructor(public mainpageservice: MainPageService,
        public router: Router,
        public toastController: ToastController,//Added
        public activateRouter: ActivatedRoute,
        public zone: NgZone,//Added
        public events: Events,
        public storage: Storage,//Added
       // private mapsAPILoader: MapsAPILoader,
       public ngZone: NgZone,
       public http: HttpClient,
       public cd: ChangeDetectorRef
    ) {
        events.subscribe('navigationExtras', (name) => {
            console.log('base constructer called');

            this.show = true;
            // alert('change'+name);
            let data: any;
            data = JSON.stringify(name);
            data = '{Filter:' + data + '}';
        });
    }

    ngOnInit(): void {
        console.log('base class Init');
    }

    loadNodes(event) {
        console.log('base class called');
        this.GetPaginatedTreeTable(event);
        //alert(event);

    }

    DropDownChange(ModuleDetailId, event, index){
        this.DropdownChange(ModuleDetailId, event, index)
    }

    GetDependentFilterDropDown(ParameterId, event, index){
        this.Getdependentfilterdropdown(ParameterId, event, index);
    }

    SubmitTreeTable(ID: any){
        this.Submittreetable(ID);
    }

    GetModulewithFilter(filter: any){
        this.GetModulewithfilter(filter);
    }

    GetDependentDropDownThroughValue(ParameterId, event, index){
        this.GetDependentDropDownThroughValue(ParameterId, event, index);
    }

    navigateOnFormSubmit(LinkedMenu) {
        let filter = localStorage.getItem("navigationExtras");
        this.events.publish('navigationExtras', JSON.parse(filter));
        let navigationExtras: NavigationExtras = {
            queryParams: JSON.parse(filter)

        };
        this.router.navigateByUrl("/menu/first/tabs/" + LinkedMenu, navigationExtras);

    }
    //Button config save for form
    UpdateData(form: any, ID: any,ButtonId:any,LinkedMenu: any) {
         this.updatedata(form, ID, ButtonId, LinkedMenu)
    }
    //Button config save for table/pivottable
    UpdateDatatable(form: any, ID: any,ButtonId:any,LinkedMenu: any) {
          this.updatedatatable(form, ID, ButtonId, LinkedMenu)
    }
    //To submit Form
    onSubmit(form: any, ID: any, LinkedMenu: any): void{
        this.onsubmit(form, ID,LinkedMenu);
    }

    HandleReaderLoaded(readerEvt){
        this.handleReaderLoaded(readerEvt);
    }

    GenerateReport(Rowval, ModuleId, ModuleDetailId, ReportType) {
        this.generatereport(Rowval, ModuleId, ModuleDetailId, ReportType)
    }

    SendReport(Rowval, ModuleId, ModuleDetailId, ReportType) {
        this.sendreport(Rowval, ModuleId, ModuleDetailId, ReportType);
    }

    ToggleOTPEmail(email: any){
        this.toggleOTPEmail(email);
    }

    CheckOTP(OTP){
        this.checkOTP(OTP);
    }

    OnWizardSubmit(ID){
        this.OnWizardSubmit(ID);
    }

    HandleFileInput(files: FileList) {
        this.handleFileInput(files);
    }

    RenderDataTable(){
        this.renderDataTable();
    }

    //To submit Form
    private  onsubmit(form: any, ID: any, LinkedMenu: any): void {
        let filter1 = localStorage.getItem('navigationExtras');
        this.router.events.pipe(filter((event: RouterEvent) => event instanceof NavigationEnd)
        ).subscribe(() => {//refresh module on reload/navigation like back or submenu click
            this.GetModulewithFilter(filter1);
        });
        const that = this;
        let Mess = undefined;
        this.UserName = localStorage.getItem('username');
        this.show = true;

        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, this.UserName).subscribe(resp => {
            console.log(JSON.stringify(resp["Message"]));
            let toast = this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',

            });
            this.GetModulewithFilter(filter1);
            if (LinkedMenu != 0 && LinkedMenu != '' && LinkedMenu != null) {
                this.navigateOnFormSubmit(LinkedMenu);
            }
            
            toast.then(toast => toast.present());
            this.show = false;

        });
    }
         
    private  updatedatatable(form: any, ID: any, ButtonId: any, LinkedMenu: any) {
            let filter1 = localStorage.getItem('navigationExtras');
            this.router.events.pipe(filter((event: RouterEvent) => event instanceof NavigationEnd)
            ).subscribe(() => {
                this.GetModulewithFilter(filter1);
            });
            const that = this;
            let Mess = undefined;
            this.UserName = localStorage.getItem('username');
            this.show = true;

            //Form Without FileUpload
            this.mainpageservice.UpdateDatatable(ID, ButtonId, form, this.UserName).subscribe(resp => {
                console.log(JSON.stringify(resp["Message"]));
                let toast = this.toastController.create({
                    message: resp["Message"],
                    duration: 3000,
                    position: 'bottom',
                    closeButtonText: 'Ok',

                });
                this.GetModulewithFilter(filter1);
                if (LinkedMenu != 0 && LinkedMenu != '' && LinkedMenu != null) {
                    this.navigateOnFormSubmit(LinkedMenu);
                }

                toast.then(toast => toast.present());
                this.show = false;

            });
    }

    private updatedata(form: any, ID: any,ButtonId:any,LinkedMenu: any) {
            let filter1 = localStorage.getItem('navigationExtras');
            this.router.events.pipe(filter((event: RouterEvent) => event instanceof NavigationEnd)
            ).subscribe(() => {
                this.GetModulewithFilter(filter1);
            });
            const that = this;
            let Mess = undefined;
            this.UserName = localStorage.getItem('username');
            this.show = true;

            //Form Without FileUpload
            this.mainpageservice.UpdateData(ID, ButtonId, form, this.UserName).subscribe(resp => {
                console.log(JSON.stringify(resp["Message"]));
                let toast = this.toastController.create({
                    message: resp["Message"],
                    duration: 3000,
                    position: 'bottom',
                    closeButtonText: 'Ok',

                });
                this.GetModulewithFilter(filter1);
                if (LinkedMenu != 0 && LinkedMenu != '' && LinkedMenu != null) {
                    this.navigateOnFormSubmit(LinkedMenu);
                }

                toast.then(toast => toast.present());
                this.show = false;

            });
    }

    replacer(key, val) {
        if (key == "_parent" || key == "parent") return undefined;
        else return val;

    }

    sortingDataAccessor(item, property) {
      
        if (property.includes('.')) {
            return property.split('.')
                .reduce((object, key) => object[key], item);
        }
        return item[property].Value;
    }//sorting nested object array

    nestedFilterCheck(search, data, key) {
        if (typeof data[key] === 'object') {
            for (const k in data[key]) {
                if (data[key][k] !== null) {
                    search = this.nestedFilterCheck(search, data[key], k);
                }
            }
        } else {
            search += data[key];
        }
        return search;
    }//searching nested object array

    private Submittreetable(ID: any) {
        var dataObject = JSON.stringify(this.files, this.replacer);
        this.mainpageservice.SaveTreetable(ID, dataObject, this.UserName).subscribe(resp => {
            //console.log(JSON.stringify(resp["Message"]));
            let toast = this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',

            });
            debugger;
            this.GetPaginatedTreeTable(filter);
            toast.then(toast => toast.present());
            this.show = false;

        });
    }

    private Getdependentfilterdropdown(ParameterId, event, index) {
        var IDvalue = event.detail.value.value == undefined ? event.detail.value.Value : event.detail.value.value;

        this.mainpageservice.getDependentFilterDropDownValue(ParameterId, IDvalue).subscribe(response => {
            debugger;
            console.log(response);
            var DropDown = document.getElementsByName(index)
            this.DropDown[index] = <any>(response)["Table"];
        }, err => {
            console.log(err);
        });

    }

    private GetdependentdropDownthroughvalue(ParameterId, event, index) {
        var IDvalue = event.detail.value.Text == undefined ? event.detail.value.Text : event.detail.value.Text;
        var trimValue = $.trim(IDvalue);
        this.mainpageservice.getDependentDropDownValue(ParameterId, trimValue).subscribe(response => {
            debugger;
            console.log(response);
            var DropDown = document.getElementsByName(index)
            this.DropDown[index] = <any>(response)["Table"];
        }, err => {
            console.log(err);
        });
    }

    private DropdownChange(ModuleDetailId, event, index) {

        if (event.value.Value != '') {
            this.mainpageservice.getDependentDropDownValue(ModuleDetailId, event.value.Value).subscribe(response => {
                debugger;
                console.log(response);
                this.DropDown[index] = <any>(response)["Table"];
            }, err => {

            });
        }

    }

    private GetPaginatedTreeTable(filter: any) {
           debugger;
        filter = JSON.stringify(filter);
        this.show = true;

        this.mainpageservice.GetPaginatedTreeTable(this.Sys_Menu_ID_Base, this.UserName, '{Filter:' + filter + '}').subscribe(data => {
            console.log("GetPaginatedTreeTable", data);
            this.Module = data;
            var columns = [];
            columns = data[0].moduleList[0].moduleDetails;
            var treedata=[];
            treedata=this.Module[0].moduleList[0].TreeOutputDataRoot;
            var temp = [];
            this.editableCols = [
                { field: 'Q1', header: 'Q1' }
              ];
            for (let i = 0; i < columns.length; i++) {
                if(columns[i].InputControls != "TextBox"){
                var  eachProduct = {
                "field":columns[i].ColumnName,
                "header":columns[i].ColumnName,
                "edit":false
                };
                temp.push(eachProduct)
            }
            else
            {
                var  eachProduct = {
                "field":columns[i].ColumnName,
                "header":columns[i].ColumnName,
                "edit":true
                };
                temp.push(eachProduct)
            }
            
              }
              console.log(temp);
              this.cols = temp;



            let file: FileSystemNode[] = [];
            
        for (let i = 0; i < (<any>treedata).data.length; i++) {
         
          let rootObj = (<any>treedata).data[i];
            let node = new FileSystemNode(rootObj, null,columns);
           
            file.push(node);
        }
                this.files=file;
          
            this.show = false;
           
        });
    }

    private   GetModulewithfilter(filter: any) {
        this.MyDataSourceArray = [];
        this.expandedElementArray = [];
        filter = JSON.stringify(filter);
        this.show = true;
        this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID_Base, this.UserName, '{Filter:' + filter + '}').subscribe(data => {
            console.log("Moduleapi", data);
            this.Module = data;
            console.log(data);
            this.show = false;
            this.MyDataSource = new MatTableDataSource();
            const rows = [];
            let count: number = 0;
            //res.forEach(element => rows.push(element, { detailRow: true, element }));
            //console.log(rows);
            this.Module[0].moduleList.forEach((val) => {
                let headerLevels: number;
                headerLevels = val.HeaderLevels;
                if (val.DisplayType === 'Report') {
                    this.displayedColumns = [];
                    this.MyDataSource = new MatTableDataSource();
                    this.MyDataSource.data = val.moduleData;
                    console.log('Report', val.moduleData)
                    this.MyDataSource.sortingDataAccessor = this.sortingDataAccessor;
                    this.MyDataSource.sort = this.sort;
                    this.MyDataSourceArray.push(this.MyDataSource);
                    debugger;
                    for (const key of Object.keys(val.moduleData[0])) {
                        this.displayedColumns.push((key));
                    }
                    this.displayedColumnArray.push(this.displayedColumns);
                    //pagination
                    let paginatorArrya = this.paginatorList.toArray();
                    //let sortarray = this.sortList.toArray();
                    let count: number = 0;
                    this.MyDataSourceArray.forEach((item) => {
                        //debugger;
                        item.paginator = paginatorArrya[count];
                        //item.sort = sortarray[count];
                        count++;
                    })

                    this.isLoadingResults = false;
                    count++;

                }
                else if (val.DisplayType === '' || val.DisplayType === 'PivotTable'||val.DisplayType==='MultiHeaderTable') {
                    this.displayedColumns = [];
                    this.MyDataSource = new MatTableDataSource();

                    let dataArray: Array<any> = [];
                    dataArray = val.ModuleDataWithRule;
                    var resultArray = Object.keys(dataArray).map(function (index) {
                        let data = dataArray[index];

                        return data;
                    });


                    this.MyDataSource.filterPredicate = (data, filter: string) => {
                        const accumulator = (currentTerm, key) => {
                            return this.nestedFilterCheck(currentTerm, data, key);
                        };
                        const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
                        // Transform the filter by converting it to lowercase and removing whitespace.
                        const transformedFilter = filter.trim().toLowerCase();
                        return dataStr.indexOf(transformedFilter) !== -1;
                    };

                    this.MyDataSource.sortingDataAccessor = this.sortingDataAccessor;
                    this.MyDataSource.sort = this.sort;
                    this.MyDataSource.data = resultArray;

                    //for (let key in val.moduleData[0]) {
                    //    this.displayedColumns.push((key));

                    //}
                    val.moduleDetails.forEach((val) => {
                        if (val.ChildColumn == false && (val.HeaderLevel == 0 || val.HeaderLevel == headerLevels)) {
                          
                        }
                        else {
                            this.childColumns.push((val.ColumnName));
                        }
                    })

                    this.displayedColumnArray.push(this.displayedColumns);
                    this.childColumnArray.push(this.childColumns);
                    let childColumns = this.childColumns;


                    var filtered = resultArray.map(function (result) {

                        let rows: Array<any> = [];
                        rows = result;
                        var rowArray = childColumns.map(function (index) {
                            let key = Object.keys(rows);

                            let data = {
                                ["" + index]: rows[index],
                            }
                            console.log(data);
                            return data;

                        });



                        let rowsvalue: any = {};
                        rowsvalue = Object.assign({}, ...rowArray);

                        return rowsvalue;
                    })


                    this.expandedElement = filtered;
                    this.expandedElementArray.push(this.expandedElement);

                    //val.moduleData.forEach(element => rows.push(element, { detailRow: true, element }));
                    //    console.log(rows);
                    this.MyDataSourceArray.push(this.MyDataSource);

                    //pagination
                    let paginatorArrya = this.paginatorList.toArray();
                    //let sortarray = this.sortList.toArray();
                    this.MyDataSourceArray.forEach((item) => {
                        //debugger;
                        item.paginator = paginatorArrya[count];
                        //item.sort = sortarray[count];
                    })

                    this.isLoadingResults = false;
                    count++;
                }
            });
        });
    }

    private  handleReaderLoaded(readerEvt) {
        var binaryString = readerEvt.target.result;
        this.id = Math.random();
        sessionStorage.setItem('id', this.id);
        this.base64textString = btoa(binaryString);
        console.log(btoa(binaryString));
        this.storage.set('sampler-' + this.id, btoa(binaryString));
        let FileType: FileObject = {
            name: this.FileName,
            FileData: this.b64toBlob(btoa(binaryString), '', '')
        };
        console.log(btoa(binaryString));

        this.storage.set('FileType-' + this.id, FileType);
        let toast = this.toastController.create({
            message: "Your image is stored. Click on Submit to save the form.",
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'Ok',

        });
        toast.then(toast => toast.present());


        console.log('blob data while passing to savedata1' + JSON.stringify(FileType));


    }

    private b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    private doAsyncGenerateReport(Rowval, ModuleId, ModuleDetailId, ReportType) {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                let data: any
                this.UserName = localStorage.getItem('username');
                data = '{Rowval:"' + Rowval + '",' + 'ModuleDetailId:"' + ModuleDetailId + '",ModuleId:"' + ModuleId + '"}';


                this.mainpageservice.GenerateReport(ReportType, this.UserName, JSON.stringify(data)).subscribe(resp => {

                    //  application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                    var blob = new Blob([resp], { type: 'application/pdf' });
                    var d = new Date();
                    var montharr = d.getMonth() + 1;
                    var month = (montharr < 10 ? '0' : '') + montharr;
                    var day = ((d.getDate()) < 10 ? '0' : '') + (d.getDate());
                    var year = d.getFullYear();
                    var x = year + '-' + month + '-' + day;
                    var filename = ReportType + x + '.pdf';

                    saveAs(blob, filename);
                    this.show = false;
                });
                resolve();
            }, 1000);
        });

        debugger;
        return promise;
    }

    private generatereport(Rowval, ModuleId, ModuleDetailId, ReportType) {
        if (Rowval == '' || typeof (Rowval) === undefined || Rowval == null) {
            let filter = localStorage.getItem("navigationExtras");
            let param = JSON.parse(filter);
            Rowval = param["rowval"];
        }


        this.doAsyncGenerateReport(Rowval, ModuleId, ModuleDetailId, ReportType).then(
            () => this.show = true,
            // () => console.log("Task Errored!"),
        );

    }

    private sendreport(Rowval, ModuleId, ModuleDetailId, ReportType) {
        let data: any
        this.UserName = localStorage.getItem('username');
        data = '{Rowval:"' + Rowval + '",' + 'ModuleDetailId:"' + ModuleDetailId + '",ModuleId:"' + ModuleId + '"}';


        this.mainpageservice.SendReport(ReportType, this.UserName, JSON.stringify(data)).subscribe(resp => {

            let toast = this.toastController.create({
                message: "Mail sent successfully!",
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',

            });
            toast.then(toast => toast.present());
            this.show = false;

        })
    }

    private toggleOTPEmail(email: any) {
        this.VerifiedEmailId = email;
        this.showContainerEmail = true;
        this.mainpageservice.GenerateOTPEmail(email).subscribe(resp => {
            alert(JSON.stringify(resp));
            let OTP = resp["OTP"];
            localStorage.setItem('OTP', OTP);

        });
    }

    private checkOTP(OTP: any) {
        let OTPValue = localStorage.getItem('OTP');
        if (OTP == OTPValue) {
            alert("Email validated");
            localStorage.removeItem('OTP');
            this.showContainerEmail = false;
            this.ValidatedEmail = true;
        }
        else {
            alert("OTP not valid");
        }
    }

    private onWizardSubmit(ID: any): void {
        debugger;
        this.UserName = localStorage.getItem('username');
        this.show = true;
        let form = JSON.stringify(this.WizardFormValue)
        //Form Without FileUpload
        this.mainpageservice.Savedata(ID, form, null).subscribe(resp => {
            console.log(JSON.stringify(resp["Message"]));
            let toast = this.toastController.create({
                message: resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',

            });
            toast.then(toast => toast.present());
            this.show = false;

        });

    }

    private handleFileInput(files: FileList) {
        this.UserName = localStorage.getItem('username');
    this.fileToUpload = files.item(0);
    //To display loader
    this.show=true;
    this.mainpageservice.postFile(this.fileToUpload, this.UserName,'FileUpload').subscribe(resp=>{
        this.show=false;
        console.log(JSON.stringify(resp["Message"]));
        //To display loader
        let toast = this.toastController.create({
          message:resp["Message"],
          duration: 3000,
          position: 'bottom',
          closeButtonText: 'Ok'
        });
        toast.then(toast => toast.present());
    });     
}

private renderDataTable() {
    this.show = true;
    this.UserName = localStorage.getItem('username');
    this.mainpageservice.GetModulesWithFilter(this.Sys_Menu_ID_Base, this.UserName, undefined)
        .subscribe(
            res => {

                debugger;
                this.MyDataSource = new MatTableDataSource();
                this.Module = res;
                let count: number = 0;
                this.Module[0].moduleList.forEach((val) => {
                    this.MyDataSource = new MatTableDataSource();
                    this.MyDataSource.data = val.moduleData;
                    this.MyDataSourceArray.push(this.MyDataSource);
                    this.isLoadingResults = false;
                    count++;
                })

            },
            error => {
                console.log('There was an error while retrieving Posts !!!' + error);
            });
}

}
