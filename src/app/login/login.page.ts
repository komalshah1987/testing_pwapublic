import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../Services/authentication.service';
import { LoginService } from '../Services/Login.service';
import { IUser } from '../Models/User';
import { Events } from '@ionic/angular';
import { Data } from "../Services/data.service";
import { error } from 'protractor';
import { EncrDecrServiceService } from '../Services/encr-decr-service.service';
import { debug } from 'util';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public User: IUser;
    public error: string;
    public show: boolean = false;

    constructor(private authentication: AuthenticationService, private loginservice: LoginService,private EncrDecr: EncrDecrServiceService,
        public events: Events,
        public data: Data) {
        this.events.publish('PageName', 'login');}

  ngOnInit() {
  }
  doAsyncTask(username,password)
        {
            let promise = new Promise((resolve, reject) => {
                 setTimeout(() => {
                    this.loginservice.getUserDetails(username, password)
                    .subscribe(data => {
                    this.User = data;
                    console.log('userdate',data);
                    localStorage.setItem('Profile_Type',data.Profile_Type);
                    localStorage.setItem('Profile_Type',data.Profile_Type);
                    localStorage.setItem('Locale',data.Locale);
                    localStorage.setItem('DateFormat',data.DateFormat);
                    localStorage.setItem('Currency',data.Currency);
                    localStorage.setItem('TimeZone',data.TimeZone);
                    localStorage.setItem('HourFormat',data.HourFormat.toString());
                    if (this.User.Error != null) {
                        this.show = false;
                        this.error = this.User.Error;
                    }
                    else {
                        this.error = null;
                        this.data.storage = {
                            "firstname": "Nic",
                            "lastname": "Raboy",
                            "address": {
                                "city": "San Francisco",
                                "state": "California"
                            }
                        }
                        this.doAsyncLogin(username,password).then(
                            () => this.show=true,
                            () => console.log("Task Errored!"),
                          );
                        //this.authentication.login(username, password);
                        // this.show = false;
                    }
                    },
                    error => {
                        this.show = false;
                        this.error = error;
                    })
                    resolve();
                   }, 1000);
                });
                this.show=false;
                return promise;
        }
        doAsyncLogin(username,password)
        {
            let promise = new Promise((resolve, reject) => {
                 setTimeout(() => {
                    this.authentication.login(username, password);
                    resolve();
                   }, 1000);
                });
                // this.show=false;
                return promise;
        }
    loginUser(e) {
        e.preventDefault();
        console.log(e);
        let token : any;
        // var username = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[0].value);
        // var password = this.EncrDecr.set('123456$#@$^@1ERF',e.target.elements[1].value);

        var username=e.target.elements[0].value;
        var password=e.target.elements[1].value;

        this.show = true;
        this.doAsyncTask(username,password).then(
            () => this.show=true,
            () => console.log("Task Errored!"),
        );
        // this.loginservice.getUserDetails(username, password)
        //     .subscribe(data => {
        //     this.User = data;
        //     localStorage.setItem('Profile_Type',data.Profile_Type);
        //     localStorage.setItem('Profile_Type',data.Profile_Type);
        //     localStorage.setItem('Locale',data.Locale);
        //     localStorage.setItem('DateFormat',data.DateFormat);
        //     localStorage.setItem('Currency',data.Currency);
        //     localStorage.setItem('TimeZone',data.TimeZone);
        //     if (this.User.Error != null) {
        //         this.show = false;
        //         this.error = this.User.Error;
        //     }
        //     else {
        //         this.error = null;
        //         this.data.storage = {
        //             "firstname": "Nic",
        //             "lastname": "Raboy",
        //             "address": {
        //                 "city": "San Francisco",
        //                 "state": "California"
        //             }
        //         }
        //         this.doAsyncTask(username,password).then(
        //             () => this.show=true,
        //             () => console.log("Task Errored!"),
        //           );
        //         //this.authentication.login(username, password);
        //         // this.show = false;
        //     }
        //     },
        //     error => {
        //         this.show = false;
        //         this.error = error;
        //     })
    }

    public type = 'password';
    public showPass = false;
    showPassword() {
        this.showPass = !this.showPass;
        if (this.showPass) {
            this.type = 'text';
        } else {
            this.type = 'password';
        }
    }
    //login(username, password) {
    //    this.authentication.login(username, password);
    //}
}
