import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HeaderPage } from './header.page';
import { Badge } from "@ionic-native/badge/ngx";
import {MatTableModule} from '@angular/material';
import { NotificationsComponent } from './notifications/notifications.component';
const routes: Routes = [
  {
    path: '',
    component: HeaderPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,    
    RouterModule.forChild(routes)
  ],
  providers: [Badge],
  entryComponents: [NotificationsComponent],
  exports: [HeaderPage, NotificationsComponent],
  declarations: [HeaderPage,NotificationsComponent]
})
export class HeaderPageModule {}
