import { Component, OnInit } from '@angular/core';
import { LoginService } from '../Services/Login.service';
import { Data } from "../Services/data.service";
import { Events } from '@ionic/angular';



@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {
  public showLogin:boolean = false;
  public type = 'password';
  public showPass = false;
  public error = " ";
    showPassword() {
    this.showPass = !this.showPass;
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  constructor(private loginservice: LoginService,
    public events: Events,
    public data: Data,
    ) { }

  resetPassword(form:any){
    console.log(form);
    let Code:any = form.code;
    let NewPassword:any = form.password;
    let testpassword2:any = form.confirmpassword;
    let resetdata = {
      NewPassword,Code
    }
    
    alert(testpassword2 +" " + NewPassword);
    console.log(NewPassword);
    debugger;
    if(NewPassword===testpassword2){
      this.error = " ";
      alert('Your password has been updated');
      let jsondata = JSON.stringify(resetdata);
      alert(jsondata);
      this.showLogin =true;
      this.loginservice.resetPassword(resetdata).subscribe(data=>{
        console.log(data);
      });
    }
    else{
      this.error = "Password is not matching."
      this.showLogin = false;
    }
  }


  ngOnInit() {
  }

}
