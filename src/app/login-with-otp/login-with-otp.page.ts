import { Component, OnInit ,ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthenticationService } from '../Services/authentication.service';
import { LoginService } from '../Services/Login.service';
import { IUser } from '../Models/User';
import { Events } from '@ionic/angular';
import { Data } from "../Services/data.service";
import { error } from 'protractor';
import {MatSnackBar} from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { resolve } from 'path';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-login-with-otp',
  templateUrl: './login-with-otp.page.html',
  styleUrls: ['./login-with-otp.page.scss'],
})
export class LoginWithOtpPage implements OnInit {
    @ViewChild('stepper') private stepper: MatStepper;
    public show: boolean = false;
    isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
    public User: IUser;
    public error: string;
    constructor(private authentication: AuthenticationService, private loginservice: LoginService,
        public events: Events,private _formBuilder: FormBuilder,
        public data: Data,private _snackBar: MatSnackBar) {
        this.events.publish('PageName', 'login');}

        
        openSnackBar(message: string, action: string) {
            this._snackBar.open(message, action, {
              duration: 5000,
              // here specify the position
              verticalPosition: 'top'
            });
        }

        
        doAsyncTask()
        {
            let promise = new Promise((resolve, reject) => {
                 setTimeout(() => {
                    this.authentication.loginWithOtp(localStorage.getItem('username'));
                    resolve();
                   }, 1000);
                });
                this.show=false;
                debugger;
                return promise;
        }

        LoginWithOTP(OTP: any) {
            debugger;
            let OTPValue = localStorage.getItem('OTP');
            if (OTP == OTPValue) {
                this.show=true;
                localStorage.removeItem('OTP');
                this.doAsyncTask().then(
                    () => this.show=true,
                    () => console.log("Task Errored!"),
                  );
              
                //this.authentication.loginWithOtp(localStorage.getItem('username'));
                this.show=false;
                
                
            }
            else {
                //alert("OTP not valid");
                this.openSnackBar("Invalid OTP",null);
            }
        }

        SendOtp(username)
    {
        
        this.loginservice.getUserDetailsWithoutPassword(username)
            .subscribe(data => {
            this.User = data;
            localStorage.setItem('Profile_Type',data.Profile_Type);
            console.log(this.User);
            localStorage.setItem('username', this.User.UserName);
            debugger;
            if (this.User.Error != null) {
                this.stepper.previous();
                this.error = this.User.Error;
                this.openSnackBar(this.error,null);
                
            }
            else {
               
                this.loginservice.GenerateOTPEmail(this.User.Email_id).subscribe(resp => {
                    alert(JSON.stringify(resp));
                    let OTP = resp["OTP"];
                    localStorage.setItem('OTP', OTP);
                    
                });
                
            }
            },
            error => {
                this.error = error;
            })
       // debugger;
        
    }

    getUserDetails(userid)
{
    
}


    


  ngOnInit() {
    this.show=false;
    debugger;
    this.firstFormGroup = this._formBuilder.group({
        firstCtrl: ['', Validators.required]
      });
      this.secondFormGroup = this._formBuilder.group({
        secondCtrl: ['', Validators.required]
      });
  }


    loginUser(e) {
        //debugger;
        e.preventDefault();
        console.log(e);
        var username = e.target.elements[0].value;
        var password = e.target.elements[1].value;
       
        this.loginservice.getUserDetails(username, password)
            .subscribe(data => {
            this.User = data;
            console.log(this.User);
            if (this.User.Error != null) {
                this.error = this.User.Error;
            }
            else {
                this.data.storage = {
                    "firstname": "Nic",
                    "lastname": "Raboy",
                    "address": {
                        "city": "San Francisco",
                        "state": "California"
                    }
                }
                this.authentication.login(username, password);
            }
            },
            error => {
                this.error = error;
            })
        


    }
    //login(username, password) {
    //    this.authentication.login(username, password);
    //}
}
