import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {MatStepperModule} from '@angular/material/stepper';
import { IonicModule } from '@ionic/angular';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { LoginWithOtpPage } from './login-with-otp.page';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';

const routes: Routes = [
  {
    path: '',
    component: LoginWithOtpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,MatStepperModule,
    MatFormFieldModule,MatInputModule,
    MatButtonModule,MatSnackBarModule,
      RouterModule.forChild(routes),
      ReactiveFormsModule
  ],
  declarations: [LoginWithOtpPage]
})
export class LoginWithOtpPageModule {}
