import { Routes } from '@angular/router';
import { MainpagePage } from './mainpage.page';
import {Menu2Page} from './menu2/menu2.component';
import {Menu3Page} from './menu3/menu3.component';
import {Menu5Page} from './menu5/menu5.component';
import {Menu6Page} from './menu6/menu6.component';
import {Menu9Page} from './menu9/menu9.component';
import {Menu10Page} from './menu10/menu10.component';
import {Menu11Page} from './menu11/menu11.component';
import {Menu12Page} from './menu12/menu12.component';
import {Menu13Page} from './menu13/menu13.component';
import {Menu17Page} from './menu17/menu17.component';
import {Menu18Page} from './menu18/menu18.component';
import {Menu19Page} from './menu19/menu19.component';
import {Menu20Page} from './menu20/menu20.component';
import {Menu21Page} from './menu21/menu21.component';
import {Menu22Page} from './menu22/menu22.component';
import {Menu23Page} from './menu23/menu23.component';
import {Menu24Page} from './menu24/menu24.component';
import {Menu25Page} from './menu25/menu25.component';
import {Menu26Page} from './menu26/menu26.component';
import {Menu28Page} from './menu28/menu28.component';
import {Menu29Page} from './menu29/menu29.component';
import {Menu31Page} from './menu31/menu31.component';
import {Menu32Page} from './menu32/menu32.component';
import {Menu39Page} from './menu39/menu39.component';
import {Menu40Page} from './menu40/menu40.component';
import {Menu41Page} from './menu41/menu41.component';
import {Menu46Page} from './menu46/menu46.component';
import {Menu47Page} from './menu47/menu47.component';
import {Menu48Page} from './menu48/menu48.component';
import {Menu50Page} from './menu50/menu50.component';
import {Menu51Page} from './menu51/menu51.component';
import {Menu52Page} from './menu52/menu52.component';
import {Menu54Page} from './menu54/menu54.component';
import {Menu55Page} from './menu55/menu55.component';
import {Menu56Page} from './menu56/menu56.component';
import {Menu63Page} from './menu63/menu63.component';
import {Menu65Page} from './menu65/menu65.component';
import {Menu67Page} from './menu67/menu67.component';
import {Menu68Page} from './menu68/menu68.component';
import {Menu69Page} from './menu69/menu69.component';
import {Menu70Page} from './menu70/menu70.component';
import {Menu71Page} from './menu71/menu71.component';
import {Menu72Page} from './menu72/menu72.component';
import {Menu74Page} from './menu74/menu74.component';
import {Menu75Page} from './menu75/menu75.component';
import {Menu77Page} from './menu77/menu77.component';
import {Menu79Page} from './menu79/menu79.component';
import {Menu80Page} from './menu80/menu80.component';
import {Menu81Page} from './menu81/menu81.component';
import {Menu82Page} from './menu82/menu82.component';
export const routes: Routes = [
{
 path: '',
 component: MainpagePage,
children: [
{
path: '2',
component: Menu2Page
},
{
path: '3',
component: Menu3Page
},
{
path: '5',
component: Menu5Page
},
{
path: '6',
component: Menu6Page
},
{
path: '9',
component: Menu9Page
},
{
path: '10',
component: Menu10Page
},
{
path: '11',
component: Menu11Page
},
{
path: '12',
component: Menu12Page
},
{
path: '13',
component: Menu13Page
},
{
path: '17',
component: Menu17Page
},
{
path: '18',
component: Menu18Page
},
{
path: '19',
component: Menu19Page
},
{
path: '20',
component: Menu20Page
},
{
path: '21',
component: Menu21Page
},
{
path: '22',
component: Menu22Page
},
{
path: '23',
component: Menu23Page
},
{
path: '24',
component: Menu24Page
},
{
path: '25',
component: Menu25Page
},
{
path: '26',
component: Menu26Page
},
{
path: '28',
component: Menu28Page
},
{
path: '29',
component: Menu29Page
},
{
path: '31',
component: Menu31Page
},
{
path: '32',
component: Menu32Page
},
{
path: '39',
component: Menu39Page
},
{
path: '40',
component: Menu40Page
},
{
path: '41',
component: Menu41Page
},
{
path: '46',
component: Menu46Page
},
{
path: '47',
component: Menu47Page
},
{
path: '48',
component: Menu48Page
},
{
path: '50',
component: Menu50Page
},
{
path: '51',
component: Menu51Page
},
{
path: '52',
component: Menu52Page
},
{
path: '54',
component: Menu54Page
},
{
path: '55',
component: Menu55Page
},
{
path: '56',
component: Menu56Page
},
{
path: '63',
component: Menu63Page
},
{
path: '65',
component: Menu65Page
},
{
path: '67',
component: Menu67Page
},
{
path: '68',
component: Menu68Page
},
{
path: '69',
component: Menu69Page
},
{
path: '70',
component: Menu70Page
},
{
path: '71',
component: Menu71Page
},
{
path: '72',
component: Menu72Page
},
{
path: '74',
component: Menu74Page
},
{
path: '75',
component: Menu75Page
},
{
path: '77',
component: Menu77Page
},
{
path: '79',
component: Menu79Page
},
{
path: '80',
component: Menu80Page
},
{
path: '81',
component: Menu81Page
},
{
path: '82',
component: Menu82Page
},
 ]
}
];
 