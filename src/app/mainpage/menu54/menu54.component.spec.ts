import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu54Component } from './menu54.component';

describe('Menu54Component', () => {
  let component: Menu54Component;
  let fixture: ComponentFixture<Menu54Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu54Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu54Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
