import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu65Component } from './menu65.component';

describe('Menu65Component', () => {
  let component: Menu65Component;
  let fixture: ComponentFixture<Menu65Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu65Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu65Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
