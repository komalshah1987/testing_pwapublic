import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu50Component } from './menu50.component';

describe('Menu50Component', () => {
  let component: Menu50Component;
  let fixture: ComponentFixture<Menu50Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu50Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu50Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
