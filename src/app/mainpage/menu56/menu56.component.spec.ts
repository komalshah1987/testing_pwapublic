import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu56Component } from './menu56.component';

describe('Menu56Component', () => {
  let component: Menu56Component;
  let fixture: ComponentFixture<Menu56Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu56Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu56Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
