import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu70Component } from './menu70.component';

describe('Menu70Component', () => {
  let component: Menu70Component;
  let fixture: ComponentFixture<Menu70Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu70Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu70Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
