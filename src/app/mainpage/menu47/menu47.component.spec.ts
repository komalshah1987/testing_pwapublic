import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu47Component } from './menu47.component';

describe('Menu47Component', () => {
  let component: Menu47Component;
  let fixture: ComponentFixture<Menu47Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu47Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu47Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
