import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu40Component } from './menu40.component';

describe('Menu40Component', () => {
  let component: Menu40Component;
  let fixture: ComponentFixture<Menu40Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu40Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu40Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
