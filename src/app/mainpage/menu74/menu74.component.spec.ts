import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu74Component } from './menu74.component';

describe('Menu74Component', () => {
  let component: Menu74Component;
  let fixture: ComponentFixture<Menu74Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu74Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu74Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
