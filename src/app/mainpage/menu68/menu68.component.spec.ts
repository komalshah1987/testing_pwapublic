import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu68Component } from './menu68.component';

describe('Menu68Component', () => {
  let component: Menu68Component;
  let fixture: ComponentFixture<Menu68Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu68Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu68Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
