import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu39Component } from './menu39.component';

describe('Menu39Component', () => {
  let component: Menu39Component;
  let fixture: ComponentFixture<Menu39Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu39Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu39Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
