import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu31Component } from './menu31.component';

describe('Menu31Component', () => {
  let component: Menu31Component;
  let fixture: ComponentFixture<Menu31Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu31Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu31Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
