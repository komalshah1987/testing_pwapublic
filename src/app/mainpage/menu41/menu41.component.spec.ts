import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu41Component } from './menu41.component';

describe('Menu41Component', () => {
  let component: Menu41Component;
  let fixture: ComponentFixture<Menu41Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu41Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu41Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
