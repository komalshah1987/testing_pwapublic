import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu80Component } from './menu80.component';

describe('Menu80Component', () => {
  let component: Menu80Component;
  let fixture: ComponentFixture<Menu80Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu80Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu80Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
