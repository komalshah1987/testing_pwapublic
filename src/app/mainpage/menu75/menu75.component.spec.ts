import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu75Component } from './menu75.component';

describe('Menu75Component', () => {
  let component: Menu75Component;
  let fixture: ComponentFixture<Menu75Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu75Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu75Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
