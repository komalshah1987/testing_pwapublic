import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu29Component } from './menu29.component';

describe('Menu29Component', () => {
  let component: Menu29Component;
  let fixture: ComponentFixture<Menu29Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu29Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu29Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
