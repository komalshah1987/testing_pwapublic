import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu79Component } from './menu79.component';

describe('Menu79Component', () => {
  let component: Menu79Component;
  let fixture: ComponentFixture<Menu79Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu79Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu79Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
