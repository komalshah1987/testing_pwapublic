import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu81Component } from './menu81.component';

describe('Menu81Component', () => {
  let component: Menu81Component;
  let fixture: ComponentFixture<Menu81Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu81Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu81Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
