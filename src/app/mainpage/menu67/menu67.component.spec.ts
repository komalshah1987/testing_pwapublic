import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu67Component } from './menu67.component';

describe('Menu67Component', () => {
  let component: Menu67Component;
  let fixture: ComponentFixture<Menu67Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu67Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu67Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
