import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu82Component } from './menu82.component';

describe('Menu82Component', () => {
  let component: Menu82Component;
  let fixture: ComponentFixture<Menu82Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu82Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu82Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
