import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu13Component } from './menu13.component';

describe('Menu13Component', () => {
  let component: Menu13Component;
  let fixture: ComponentFixture<Menu13Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu13Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu13Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
