import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu52Component } from './menu52.component';

describe('Menu52Component', () => {
  let component: Menu52Component;
  let fixture: ComponentFixture<Menu52Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu52Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu52Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
