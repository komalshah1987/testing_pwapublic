import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu72Component } from './menu72.component';

describe('Menu72Component', () => {
  let component: Menu72Component;
  let fixture: ComponentFixture<Menu72Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu72Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu72Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
