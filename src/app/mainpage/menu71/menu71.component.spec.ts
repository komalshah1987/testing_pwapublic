import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu71Component } from './menu71.component';

describe('Menu71Component', () => {
  let component: Menu71Component;
  let fixture: ComponentFixture<Menu71Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu71Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu71Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
