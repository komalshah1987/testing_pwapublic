import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import {Menu2Page} from './menu2/menu2.component';
import {Menu3Page} from './menu3/menu3.component';
import {Menu5Page} from './menu5/menu5.component';
import {Menu6Page} from './menu6/menu6.component';
import {Menu9Page} from './menu9/menu9.component';
import {Menu10Page} from './menu10/menu10.component';
import {Menu11Page} from './menu11/menu11.component';
import {Menu12Page} from './menu12/menu12.component';
import {Menu13Page} from './menu13/menu13.component';
import {Menu17Page} from './menu17/menu17.component';
import {Menu18Page} from './menu18/menu18.component';
import {Menu19Page} from './menu19/menu19.component';
import {Menu20Page} from './menu20/menu20.component';
import {Menu21Page} from './menu21/menu21.component';
import {Menu22Page} from './menu22/menu22.component';
import {Menu23Page} from './menu23/menu23.component';
import {Menu24Page} from './menu24/menu24.component';
import {Menu25Page} from './menu25/menu25.component';
import {Menu26Page} from './menu26/menu26.component';
import {Menu28Page} from './menu28/menu28.component';
import {Menu29Page} from './menu29/menu29.component';
import {Menu31Page} from './menu31/menu31.component';
import {Menu32Page} from './menu32/menu32.component';
import {Menu39Page} from './menu39/menu39.component';
import {Menu40Page} from './menu40/menu40.component';
import {Menu41Page} from './menu41/menu41.component';
import {Menu46Page} from './menu46/menu46.component';
import {Menu47Page} from './menu47/menu47.component';
import {Menu48Page} from './menu48/menu48.component';
import {Menu50Page} from './menu50/menu50.component';
import {Menu51Page} from './menu51/menu51.component';
import {Menu52Page} from './menu52/menu52.component';
import {Menu54Page} from './menu54/menu54.component';
import {Menu55Page} from './menu55/menu55.component';
import {Menu56Page} from './menu56/menu56.component';
import {Menu63Page} from './menu63/menu63.component';
import {Menu65Page} from './menu65/menu65.component';
import {Menu67Page} from './menu67/menu67.component';
import {Menu68Page} from './menu68/menu68.component';
import {Menu69Page} from './menu69/menu69.component';
import {Menu70Page} from './menu70/menu70.component';
import {Menu71Page} from './menu71/menu71.component';
import {Menu72Page} from './menu72/menu72.component';
import {Menu74Page} from './menu74/menu74.component';
import {Menu75Page} from './menu75/menu75.component';
import {Menu77Page} from './menu77/menu77.component';
import {Menu79Page} from './menu79/menu79.component';
import {Menu80Page} from './menu80/menu80.component';
import {Menu81Page} from './menu81/menu81.component';
import {Menu82Page} from './menu82/menu82.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { IonicModule } from '@ionic/angular';
import { routes } from './mainpage.routes';
import { IonicSelectableModule } from 'ionic-selectable';
// import { MatTableModule, MatSortModule } from '@angular/material';
// import { CdkTableModule } from '@angular/cdk/table';
//import { AgGridModule } from 'ag-grid-angular';
import { MainpagePage } from './mainpage.page';
import {CustomService} from '../Services/custom-service.service'
import { Data } from '../Services/data.service';
// import { MatPaginatorModule } from '@angular/material';
import { IonicStorageModule } from '@ionic/storage';
import { SharedModule } from '../shared/shared.module';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {
  MatTableModule, MatSortModule, MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatExpansionModule,
  MatButtonToggleModule,MatNativeDateModule,
} from '@angular/material';
//import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatRadioModule} from '@angular/material/radio';
import {MatGridListModule} from '@angular/material/grid-list';
import { CdkTableModule } from '@angular/cdk/table';
//import { AgmCoreModule } from '@agm/core';
//import{GoogleMapLatLongComponent} from '../google-map-lat-long/google-map-lat-long.component';
import {TableModule} from 'primeng/Table';
import {PaginatorModule} from 'primeng/paginator';
import {DropdownModule} from 'primeng/dropdown';        //primeng select module
import {MultiSelectModule} from 'primeng/multiselect';  //pmg multiselect
import {AccordionModule} from 'primeng/accordion';      //pmg accordion necessary
import {TreeTableModule} from 'primeng/treetable';
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import {CheckboxModule} from 'primeng/checkbox';
import {RadioButtonModule} from 'primeng/radiobutton';
import {FileUploadModule} from 'primeng/fileupload';
import {ButtonModule} from 'primeng/button';
import {PanelModule} from 'primeng/panel';
import {InputNumberModule} from 'primeng/inputnumber';
import {MenubarModule} from 'primeng/menubar';
import {PanelMenuModule} from 'primeng/panelmenu';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {CardModule} from 'primeng/card';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
      IonicModule,
      RouterModule.forChild(routes),
      DataTablesModule,
      IonicSelectableModule,      
      MatTableModule, MatSortModule, MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
      MatPaginatorModule,
      MatRippleModule,
      MatCardModule,
      MatProgressSpinnerModule,
      MatButtonToggleModule,
      MatIconModule,
      CdkTableModule,
      SharedModule,
      MatGridListModule,
      MatRadioModule,
      ReactiveFormsModule,MatExpansionModule,
      MatStepperModule,MatDatepickerModule,MatNativeDateModule,
       MultiSelectModule,
      AccordionModule,
      DropdownModule,CardModule,
      PanelMenuModule,OverlayPanelModule,
      
      MenubarModule,BreadcrumbModule,
      TableModule,PaginatorModule,TreeTableModule,InputNumberModule,PanelModule,ButtonModule,FileUploadModule,
     RadioButtonModule,CheckboxModule,InputTextModule,CalendarModule,//MatMomentDateModule,
      IonicStorageModule.forRoot()
      /* AgmCoreModule.forRoot({
        apiKey: 'AIzaSyBrfK8btHhw4Xfc9EjNJjwYilg0t0s2DcI',//api key
        libraries: ['places']
       }) */

    ],
    providers: [Data,CustomService
//,
      //{ provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } } 
],
  
declarations: [MainpagePage,Menu2Page,Menu3Page,Menu5Page,Menu6Page,Menu9Page,Menu10Page,Menu11Page,Menu12Page,Menu13Page,
  Menu17Page,Menu18Page,Menu19Page,Menu20Page,Menu21Page,Menu22Page,Menu23Page,Menu24Page,Menu25Page,Menu26Page,Menu28Page,
  Menu29Page,Menu31Page,Menu32Page,Menu39Page,Menu40Page,Menu41Page,Menu46Page,Menu47Page,Menu48Page,Menu50Page,Menu51Page,Menu52Page,
  Menu54Page,Menu55Page,Menu56Page,Menu63Page,Menu65Page,Menu67Page,Menu68Page,Menu69Page,Menu70Page,Menu71Page,Menu72Page,
  Menu74Page,Menu75Page,Menu77Page,Menu79Page,Menu80Page,Menu81Page,Menu82Page],
    entryComponents: []
})
export class MainpagePageModule {}
