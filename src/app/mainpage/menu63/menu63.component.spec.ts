import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu63Component } from './menu63.component';

describe('Menu63Component', () => {
  let component: Menu63Component;
  let fixture: ComponentFixture<Menu63Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu63Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu63Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
