import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu48Component } from './menu48.component';

describe('Menu48Component', () => {
  let component: Menu48Component;
  let fixture: ComponentFixture<Menu48Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu48Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu48Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
