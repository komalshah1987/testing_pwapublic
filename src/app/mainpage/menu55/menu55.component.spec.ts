import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu55Component } from './menu55.component';

describe('Menu55Component', () => {
  let component: Menu55Component;
  let fixture: ComponentFixture<Menu55Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu55Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu55Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
