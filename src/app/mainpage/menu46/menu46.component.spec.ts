import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu46Component } from './menu46.component';

describe('Menu46Component', () => {
  let component: Menu46Component;
  let fixture: ComponentFixture<Menu46Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu46Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu46Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
