import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu19Component } from './menu19.component';

describe('Menu19Component', () => {
  let component: Menu19Component;
  let fixture: ComponentFixture<Menu19Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu19Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu19Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
