import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu51Component } from './menu51.component';

describe('Menu51Component', () => {
  let component: Menu51Component;
  let fixture: ComponentFixture<Menu51Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu51Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu51Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
