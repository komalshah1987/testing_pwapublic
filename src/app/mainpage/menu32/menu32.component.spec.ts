import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Menu32Component } from './menu32.component';

describe('Menu32Component', () => {
  let component: Menu32Component;
  let fixture: ComponentFixture<Menu32Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Menu32Component ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Menu32Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
