import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from, of, forkJoin } from 'rxjs';
import { switchMap, finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

import { MainPageService } from '../Services/MainPage.service';
//import { environment } from '../../environments/environment';

const STORAGE_REQ_KEY = 'storedreq';
//const apiUrl = environment.apiUrl;
interface FileObject {
    name: string,
    FileData: Blob
}

interface StoredRequest {
    url: string,
    type: string,
    data: any,
    headers: any,
    body: any,
    id: any
}


@Injectable({
    providedIn: 'root'
})
export class OfflineManagerService {

    constructor(private storage: Storage, private http: HttpClient, public mainpageservice: MainPageService, private toastController: ToastController) { }

    checkForEvents(): Observable<any> {
        debugger;
        return from(this.storage.get(STORAGE_REQ_KEY)).pipe(
            switchMap(storedOperations => {
                let storedObj = JSON.parse(storedOperations);
                if (storedObj && storedObj.length > 0) {
                    console.log('Local data succesfully synced to API!');
                    console.log(storedObj);
                    return this.sendRequests(storedObj).pipe(
                        finalize(() => {
                            let toast = this.toastController.create({
                                message: `Local data succesfully synced to API!`,
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.then(toast => toast.present());

                            this.storage.remove(STORAGE_REQ_KEY);
                        })
                    );
                } else {
                    console.log('no local events to sync');
                    return of(false);
                }
            })
        )
    }
    checkForEventsOnClick(): Observable<any> {
        debugger;
        return from(this.storage.get(STORAGE_REQ_KEY)).pipe(
            switchMap(storedOperations => {
                let storedObj = JSON.parse(storedOperations);

                if (storedObj && storedObj.length > 0) {
                    console.log('Local data succesfully synced to API!');
                    console.log(storedObj);

                    return this.sendRequests(storedObj).pipe(
                        finalize(() => {
                            let toast = this.toastController.create({
                                message: `Local data succesfully synced to API!`,
                                duration: 3000,
                                position: 'bottom'
                            });
                            toast.then(toast => toast.present());

                            this.storage.remove(STORAGE_REQ_KEY);
                        })
                    );
                } else {
                    console.log('no local events to sync');
                    let toast = this.toastController.create({
                        message: `No local events to sync`,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.then(toast => toast.present());
                    return of(false);
                }
            })
        )
    }

    storeRequest(url, type, data, id) {
        let toast = this.toastController.create({
            message: `Your data is stored locally because you seem to be offline.`,
            duration: 3000,
            position: 'bottom'
        });
        toast.then(toast => toast.present());

        let action: StoredRequest = {
            url: url,
            type: type,
            data: data,
            body: data,
            id: id,
            headers: '{"Accept":"application/json, text/plain, */*","Content-Type":"multipart/form-data"}'


            //time: new Date().getTime(),
            //id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
        };
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript

        return this.storage.get(STORAGE_REQ_KEY).then(storedOperations => {
            let storedObj = JSON.parse(storedOperations);

            if (storedObj) {
                console.log('storage :' + storedObj);
                storedObj.push(action);
            } else {
                storedObj = [action];
            }
            // Save old & new local transactions back to Storage
            return this.storage.set(STORAGE_REQ_KEY, JSON.stringify(storedObj));
        });
    }

    sendRequests(operations: StoredRequest[]) {
        let obs = [];
        debugger;
        var data;
        var httpdata;
        var form1 = new FormData();

        this.storage.get('FileType').then((FileType) => {
            httpdata = FileType;
            form1.append(FileType.name, FileType.data);
        });
        this.storage.get('data').then((storeddata) => {
            data = JSON.stringify(storeddata);
            form1.append('data', JSON.stringify(storeddata));

            if (httpdata != undefined && data != undefined) {
                /*  let oneObs =
                 this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com',httpdata ).subscribe(response => {});
                  //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                 //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                 obs.push(oneObs);  */
            }
        });
        for (let op of operations) {

            console.log(op.id);
            this.storage.get('FileType-' + op.id).then((FileType) => {
                httpdata = FileType;
                form1.append(FileType.name, FileType.data);
                this.storage.get('data-' + op.id).then((storeddata) => {
                    data = JSON.stringify(storeddata);
                    form1.append('data', JSON.stringify(storeddata));

                    if (httpdata != undefined && data != undefined) {
                        console.log(httpdata);
                        console.log(data);
                        let oneObs =
                            this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com', httpdata).subscribe(response => { });
                        //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                        //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                        obs.push(oneObs);
                    }
                });

            });

        }

        // Send out all local events and return once they are finished
        return forkJoin(obs);
    }

    storeRequest1(url, type, data, id) {
        let toast = this.toastController.create({
            message: `Your data is stored locally because you seem to be offline.`,
            duration: 3000,
            position: 'bottom'
        });
        toast.then(toast => toast.present());

        let action: StoredRequest = {
            url: url,
            type: type,
            data: data,
            body: data,
            id: id,
            headers: '{"Accept":"application/json, text/plain, */*","Content-Type":"multipart/form-data"}'


            //time: new Date().getTime(),
            //id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
        };
        // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript

        return this.storage.get(STORAGE_REQ_KEY).then(storedOperations => {
            let storedObj = JSON.parse(storedOperations);

            if (storedObj) {
                console.log('storage :' + storedObj);
                storedObj.push(action);
            } else {
                storedObj = [action];
            }
            // Save old & new local transactions back to Storage
            return this.storage.set(STORAGE_REQ_KEY, JSON.stringify(storedObj));
        });
    }

    sendRequests1(operations: StoredRequest[]) {
        let obs = [];
        debugger;
        var data;
        var httpdata;
        var form1 = new FormData();

        this.storage.get('FileType').then((FileType) => {
            httpdata = FileType;
            form1.append(FileType.name, FileType.data);
        });
        this.storage.get('data').then((storeddata) => {
            data = JSON.stringify(storeddata);
            form1.append('data', JSON.stringify(storeddata));

            if (httpdata != undefined && data != undefined) {
                let oneObs =
                    this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com', httpdata).subscribe(response => { });
                //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
                //= this.http.request(op.type, op.url, data:form1, { body:form1 });
                obs.push(oneObs);
            }
        });
        for (let op of operations) {
            /* console.log('Make one request: ', op);
        
           var newdata = this.getLocal('data');
           var newhttpdata = this.getLocal('FileType');
            console.log(form1);
            if(httpdata != undefined && data != undefined){
            let oneObs =
            this.mainpageservice.Savedata1(37, data, 'samadhan@gmail.com',httpdata )
             //this.http.post(`${apiUrl}/${'api/Page/SaveModules?id='+37+'&username=' +'samadhan@gmail.com'}`,form1);
            //= this.http.request(op.type, op.url, data:form1, { body:form1 });
            obs.push(oneObs); 
            } */
            /* this.mainpageservice.Savedata1(37,"TestFromOfflineManager", 'samadhan@gmail.com', op.data).subscribe(resp=>{
      
              console.log(JSON.stringify(resp["Message"]));
              let toast = this.toastController.create({
                message:resp["Message"],
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Ok',
        
              });
              toast.then(toast => toast.present());
            }); */
        }

        // Send out all local events and return once they are finished
        return forkJoin(obs);
    }

    getLocal(key) {
        console.log('getlocaldata' + this.storage.get(`${key}`));
        var getdata = this.storage.get(`${key}`).then
        return this.storage.get(`${key}`);

    }

    public getStoredData() {
        return this.storage.get('data').then((val) => { // <-- Here!
            console.dir(val);
            return val;
        });
    }
}

