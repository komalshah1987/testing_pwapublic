import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { Events } from '@ionic/angular';
import { DashboardPage } from './dashboard.page';
import { SharedModule } from '../shared/shared.module';
// import { Menu149Page } from '../mainpage/menu149/menu149.component';
// import { Menu151Page } from '../mainpage/menu151/menu151.component';
// import { Menu152Page } from '../mainpage/menu152/menu152.component';
//import { DashboardAuthGuard } from '../Services/dashboard-auth-guard.service';
import { MainPageService } from '../Services/MainPage.service';
import {
  MatTableModule, MatSortModule, MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatIconModule,
  MatExpansionModule,
  MatButtonToggleModule
} from '@angular/material';
import {IonicStorageModule} from '@ionic/storage';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  },
//     {
//       path: '149',
//       component: Menu149Page,
//     // canActivate:[DashboardAuthGuard]
//     },
//     {
//       path: '151',
//       component: Menu151Page,
//    // canActivate:[DashboardAuthGuard]
//   },
//   {
//     path: '152',
//     component: Menu152Page,
//   //canActivate:[DashboardAuthGuard]
// }
    
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule, MatSortModule, MatButtonModule,
      MatFormFieldModule,
      MatInputModule,
    SharedModule,
      MatPaginatorModule,
      MatRippleModule,
      MatCardModule,
      MatProgressSpinnerModule,
      MatButtonToggleModule,MatIconModule,
      MatExpansionModule,
    RouterModule.forChild(routes),
    IonicStorageModule.forRoot(),
  ],
  declarations: [DashboardPage],
  providers:[]
})
export class DashboardPageModule{
  
}
